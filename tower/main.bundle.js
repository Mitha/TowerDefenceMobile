webpackJsonp([1,4],{

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__monster_monster_enum__ = __webpack_require__(478);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GameService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GameService = (function () {
    function GameService() {
    }
    GameService.prototype.ngOnInit = function () {
    };
    GameService.prototype.earnMoney = function (monster) {
        var money = 20;
        if (monster.type === __WEBPACK_IMPORTED_MODULE_1__monster_monster_enum__["a" /* MonsterEnum */].MONSTER_2) {
            money = 30;
        }
        else if (monster.type === __WEBPACK_IMPORTED_MODULE_1__monster_monster_enum__["a" /* MonsterEnum */].MONSTER_3) {
            money = 40;
        }
        this.game.money = this.game.money + money;
    };
    GameService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* Injectable */])(), 
        __metadata('design:paramtypes', [])
    ], GameService);
    return GameService;
}());
//# sourceMappingURL=game.service.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SquareEnum; });
var SquareEnum = {
    TYPE_PATH: "path",
    TYPE_BEGIN: "begin",
    TYPE_END: "end",
    TYPE_STONE: "stone",
    DIRECTION_LEFT: "left",
    DIRECTION_TOP: "top",
    DIRECTION_BOTTOM: "bottom",
    DIRECTION_RIGHT: "right"
};
//# sourceMappingURL=square.enum.js.map

/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__square_square_enum__ = __webpack_require__(138);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SquareService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SquareService = (function () {
    function SquareService() {
        this.STEP = 3;
    }
    /**
     * Get direction of next square
     * @param  {Square} currentSquare [description]
     * @param  {Square} nextSquare    [description]
     * @return {string} direction of next square
     */
    SquareService.prototype.getDirectionNextSquare = function (currentSquare, nextSquare) {
        var direction = __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_RIGHT;
        if (nextSquare.x < currentSquare.x) {
            direction = __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_LEFT;
        }
        else if (nextSquare.y < currentSquare.y) {
            direction = __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_TOP;
        }
        else if (nextSquare.y > currentSquare.y) {
            direction = __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_BOTTOM;
        }
        return direction;
    };
    /**
     * Get degrees for next square
     * @param  {string} direction [description]
     * @return {number} degrees
     */
    SquareService.prototype.getDegreesNextSquare = function (direction) {
        var deg = 0;
        if (direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_LEFT) {
            deg = 180;
        }
        else if (direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_TOP) {
            deg = 270;
        }
        if (direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_BOTTOM) {
            deg = 90;
        }
        return deg;
    };
    SquareService.prototype.convertDirectionForPropretiesCSSUse = function (direction) {
        var newDirection = __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_LEFT;
        if (direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_TOP || direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_BOTTOM) {
            newDirection = __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_TOP;
        }
        return newDirection;
    };
    SquareService.prototype.convertSizeDirection = function (direction, distance) {
        var newDistance = distance + this.STEP;
        if (direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_LEFT) {
            newDistance = distance - this.STEP;
        }
        else if (direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_TOP) {
            newDistance = distance - this.STEP;
        }
        else if (direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_BOTTOM) {
            newDistance = distance + this.STEP;
        }
        return newDistance;
    };
    /**
     * Initialisation start position of monster in square
     * @param  {string} direction [description]
     * @param  {number} size      [description]
     * @return {number} position of monster
     */
    SquareService.prototype.initStartPositionOfMonsterInSquare = function (direction, size) {
        var position = size;
        if (direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_RIGHT || direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_BOTTOM) {
            position = -size;
        }
        return position;
    };
    /**
     * Know if monster is nearby character
     * @param  {Square} currentSquare [description]
     * @param  {Square} squareMonster [description]
     * @return true if monster is nearby character
     */
    SquareService.prototype.isInPerimeterCharacter = function (currentSquare, squareMonster) {
        var response = false;
        if ((currentSquare.x === squareMonster.x + 1 || currentSquare.x === squareMonster.x - 1 || currentSquare.x === squareMonster.x)
            && (currentSquare.y === squareMonster.y + 1 || currentSquare.y === squareMonster.y - 1 || currentSquare.y === squareMonster.y)) {
            response = true;
        }
        return response;
    };
    SquareService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* Injectable */])(), 
        __metadata('design:paramtypes', [])
    ], SquareService);
    return SquareService;
}());
//# sourceMappingURL=square.service.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__square_square_enum__ = __webpack_require__(138);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CharacterService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var COST_CHARACTER_MINI_1 = 50;
var COST_CHARACTER_MINI_2 = 100;
var COST_CHARACTER_MINI_3 = 200;
var TYPE_MINI = "mini";
var CharacterService = (function () {
    function CharacterService() {
    }
    /**
     * Calculate angle to character for monster
     * @param  {Square}  currentSquare [description]
     * @param  {Square}  ciblingSquare [description]
     * @param  {Monster} monster       [description]
     * @return {number} degrees
     */
    CharacterService.prototype.calculateAngleDegrees = function (currentSquare, ciblingSquare, monster) {
        var x = ciblingSquare.x;
        var y = ciblingSquare.y;
        // Calculate marge to remove at direction character
        var margeMonster = (currentSquare.size / 2) + (monster.size / 2);
        // Calculate position of monster in square
        var temp = (monster.distance - margeMonster) / currentSquare.size;
        // Manage direction of character by monster direction
        if (ciblingSquare.direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_RIGHT) {
            x = x + temp;
        }
        else if (ciblingSquare.direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_LEFT) {
            x = x - temp;
        }
        else if (ciblingSquare.direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_TOP) {
            y = y - temp;
        }
        else if (ciblingSquare.direction === __WEBPACK_IMPORTED_MODULE_1__square_square_enum__["a" /* SquareEnum */].DIRECTION_BOTTOM) {
            y = y + temp;
        }
        // Calculate angle
        var angle = Math.atan2(currentSquare.x - x, currentSquare.y - y) * 180 / Math.PI;
        return -angle;
    };
    /**
     * Get price of character
     * @param  {Character} character [description]
     * @return {number}              [description]
     */
    CharacterService.prototype.buyCharacter = function (character) {
        var price = COST_CHARACTER_MINI_1;
        // Type mini
        if (character.type === TYPE_MINI) {
            if (character.level === 2) {
                price = COST_CHARACTER_MINI_2;
            }
            else if (character.level === 3) {
                price = COST_CHARACTER_MINI_3;
            }
        }
        return price;
    };
    CharacterService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* Injectable */])(), 
        __metadata('design:paramtypes', [])
    ], CharacterService);
    return CharacterService;
}());
//# sourceMappingURL=character.service.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MapService = (function () {
    function MapService(http) {
        this.http = http;
        this.map = [];
        this.mapUrl = "" + __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].map_url;
        this.envName = "" + __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].envName;
    }
    MapService.prototype.ngOnInit = function () {
    };
    MapService.prototype.getMap = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers });
        return this.http.get(this.mapUrl, options)
            .map(this.extractData)
            .catch(this.handleError);
    };
    MapService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    MapService.prototype.handleError = function (error) {
        // In a real world app, you might use a remote logging infrastructure
        var errMsg;
        if (error instanceof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Response */]) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].throw(errMsg);
    };
    MapService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* Http */]) === 'function' && _a) || Object])
    ], MapService);
    return MapService;
    var _a;
}());
//# sourceMappingURL=map.service.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Square; });
var Square = (function () {
    function Square(type, x, y, size, padding, style) {
        this.type = type;
        this.x = x;
        this.y = y;
        this.size = size;
        this.padding = padding;
        this.lastDirection = null;
        this.nextDirection = null;
        this.directionProperties = null;
        this.style = style;
    }
    return Square;
}());
//# sourceMappingURL=square.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataSubject; });
var DataSubject = (function () {
    function DataSubject(action, data, condition) {
        this.action = action;
        this.data = data;
        this.condition = condition;
    }
    return DataSubject;
}());
//# sourceMappingURL=data.subject.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Character; });
var Character = (function () {
    function Character(type, level, damage, rotation, speed, distance, select, position, style) {
        this.type = type;
        this.level = level;
        this.damage = damage;
        this.rotation = 0;
        this.speed = speed;
        this.distance = distance;
        this.select = select;
        this.position = position;
        this.style = style;
    }
    return Character;
}());
//# sourceMappingURL=character.js.map

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MonsterSound; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MonsterSound = (function () {
    function MonsterSound() {
    }
    MonsterSound.prototype.soundHurt = function () {
        var audio = new Audio("./sound/hurt.mp3");
        audio.play();
    };
    MonsterSound = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* Injectable */])(), 
        __metadata('design:paramtypes', [])
    ], MonsterSound);
    return MonsterSound;
}());
//# sourceMappingURL=monster.sound.js.map

/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Monster; });
var Monster = (function () {
    function Monster(type, level, health, rotation, speed, position) {
        this.type = type;
        this.level = level;
        this.rotation = rotation;
        this.speed = speed;
        this.position = position;
        this.health = health;
        this.top = 0;
        this.left = 0;
        this.right = 0;
        this.bottom = 0;
        this.distance = 0;
        this.id = this.generatorID();
        this.square = null;
    }
    Monster.prototype.getRotateCSS = function () {
        return "rotate(" + this.rotation + "deg)";
    };
    /**
     * Generate ID of monster
     * @return {[type]} [description]
     */
    Monster.prototype.generatorID = function () {
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    };
    return Monster;
}());
//# sourceMappingURL=monster.js.map

/***/ }),

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var BASE_URL = 'http://localhost:3000';
var TOWER_SERVER_URL = "http://localhost:4201";
var environment = {
    production: false,
    map_url_1: TOWER_SERVER_URL + "/maps/1",
    map_url_2: TOWER_SERVER_URL + "/maps/2",
    map_url_3: TOWER_SERVER_URL + "/maps/3",
    map_url: BASE_URL + "/towerDefence",
    envName: 'Develop',
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 362:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 362;


/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(450);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(471);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(318);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 470:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__util_data_subject_util__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__game_game_service__ = __webpack_require__(137);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = (function () {
    function AppComponent() {
        this.title = 'Tower Defence';
    }
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__(544),
            styles: [__webpack_require__(534)],
            providers: [__WEBPACK_IMPORTED_MODULE_1__util_data_subject_util__["a" /* DataSubjectUtil */], __WEBPACK_IMPORTED_MODULE_2__game_game_service__["a" /* GameService */]]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 471:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(441);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(470);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__character_character_component__ = __webpack_require__(472);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__monster_monster_component__ = __webpack_require__(477);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__game_game_component__ = __webpack_require__(474);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__map_map_component__ = __webpack_require__(476);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__square_square_component__ = __webpack_require__(479);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_5__character_character_component__["a" /* CharacterComponent */],
                __WEBPACK_IMPORTED_MODULE_6__monster_monster_component__["a" /* MonsterComponent */],
                __WEBPACK_IMPORTED_MODULE_7__game_game_component__["a" /* GameComponent */],
                __WEBPACK_IMPORTED_MODULE_8__map_map_component__["a" /* MapComponent */],
                __WEBPACK_IMPORTED_MODULE_9__square_square_component__["a" /* SquareComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 472:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__square_square__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__character_character__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__character_character_service__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__util_data_subject_util__ = __webpack_require__(69);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CharacterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var REFRESH_TIME = 40;
var MAX_LEVEL = 3;
var CharacterComponent = (function () {
    function CharacterComponent(dataSubjectUtil, characterService) {
        this.dataSubjectUtil = dataSubjectUtil;
        this.characterService = characterService;
        this.notifyParent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* EventEmitter */]();
        this.assignMonster = null;
    }
    CharacterComponent.prototype.ngOnInit = function () {
        this.handleParent();
    };
    CharacterComponent.prototype.handleParent = function () {
        var _this = this;
        this.parentSubject.subscribe(function (event) {
            if (event.action === "addLevel") {
                _this.addLevel();
            }
            else if (event.action === "attackMonster") {
                if (_this.assignMonster !== null) {
                    _this.attackMonster();
                }
                else {
                    _this.assignMonster = event.data;
                }
            }
            else if (event.action === "attachMonster") {
                _this.assignMonster = event.data;
            }
            else if (event.action === "detachMonster") {
                _this.assignMonster = null;
            }
            else if (event.action === "manageCharacter" && _this.assignMonster !== null) {
                var angle = _this.characterService.calculateAngleDegrees(_this.square, _this.assignMonster.square, _this.assignMonster);
                _this.character.rotation = angle;
            }
        });
    };
    CharacterComponent.prototype.controlleCharacter = function () {
    };
    CharacterComponent.prototype.changePosition = function () {
    };
    CharacterComponent.prototype.eventTime = function () {
        setTimeout(this.controlleCharacter(), REFRESH_TIME);
    };
    CharacterComponent.prototype.addLevel = function () {
        if (this.character.level < MAX_LEVEL) {
            this.character.level++;
            this.character.style = this.getClassStyleCharacter();
        }
    };
    CharacterComponent.prototype.getClassStyleCharacter = function () {
        return "character -" + this.character.type + "-" + this.character.level;
    };
    CharacterComponent.prototype.initCharacter = function () {
        this.character.type = "mini";
        this.character.level = 1;
        this.character.distance = 200;
    };
    CharacterComponent.prototype.attackMonster = function () {
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__character_character__["a" /* Character */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__character_character__["a" /* Character */]) === 'function' && _a) || Object)
    ], CharacterComponent.prototype, "character", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])(), 
        __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4_rxjs__["Subject"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_rxjs__["Subject"]) === 'function' && _b) || Object)
    ], CharacterComponent.prototype, "parentSubject", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Output */])(), 
        __metadata('design:type', Object)
    ], CharacterComponent.prototype, "notifyParent", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])(), 
        __metadata('design:type', (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__square_square__["a" /* Square */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__square_square__["a" /* Square */]) === 'function' && _c) || Object)
    ], CharacterComponent.prototype, "square", void 0);
    CharacterComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'app-character',
            template: __webpack_require__(545),
            styles: [__webpack_require__(535)]
        }), 
        __metadata('design:paramtypes', [(typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__util_data_subject_util__["a" /* DataSubjectUtil */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__util_data_subject_util__["a" /* DataSubjectUtil */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__character_character_service__["a" /* CharacterService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__character_character_service__["a" /* CharacterService */]) === 'function' && _e) || Object])
    ], CharacterComponent);
    return CharacterComponent;
    var _a, _b, _c, _d, _e;
}());
//# sourceMappingURL=character.component.js.map

/***/ }),

/***/ 473:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Fire; });
var Fire = (function () {
    function Fire(size) {
        this.size = size;
    }
    return Fire;
}());
//# sourceMappingURL=fire.js.map

/***/ }),

/***/ 474:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__map_map_service__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__util_data_subject_util__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__square_square_service__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__game_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__game__ = __webpack_require__(475);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GameComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var START_MONEY = 50;
var HEALTH = 5;
var GameComponent = (function () {
    function GameComponent(dataSubjectUtil, gameService) {
        this.dataSubjectUtil = dataSubjectUtil;
        this.gameService = gameService;
        this.listCharacter = [];
        this.listMonster = [];
        this.listPath = [];
        this.mapSubject = new __WEBPACK_IMPORTED_MODULE_6_rxjs__["Subject"]();
        this.gameService.game = new __WEBPACK_IMPORTED_MODULE_5__game__["a" /* Game */](START_MONEY, HEALTH);
        this.successGame = false;
    }
    GameComponent.prototype.ngOnInit = function () {
    };
    GameComponent.prototype.handleChildMap = function (event) {
        if (event.action === "damageMonster") {
            this.gameService.game.health--;
            // If health is null, stop game
            if (this.gameService.game.health === 0) {
                this.notifyMap(this.dataSubjectUtil.generateDataSubject("endGame", null, null));
            }
        }
        else if (event.action === "successGame") {
            this.successGame = true;
        }
    };
    GameComponent.prototype.notifyMap = function (event) {
        this.mapSubject.next(event);
    };
    GameComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'app-game',
            template: __webpack_require__(546),
            styles: [__webpack_require__(536)],
            providers: [__WEBPACK_IMPORTED_MODULE_1__map_map_service__["a" /* MapService */], __WEBPACK_IMPORTED_MODULE_2__util_data_subject_util__["a" /* DataSubjectUtil */], __WEBPACK_IMPORTED_MODULE_3__square_square_service__["a" /* SquareService */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__util_data_subject_util__["a" /* DataSubjectUtil */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__util_data_subject_util__["a" /* DataSubjectUtil */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__game_service__["a" /* GameService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__game_service__["a" /* GameService */]) === 'function' && _b) || Object])
    ], GameComponent);
    return GameComponent;
    var _a, _b;
}());
//# sourceMappingURL=game.component.js.map

/***/ }),

/***/ 475:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Game; });
var Game = (function () {
    function Game(money, health) {
        this.money = money;
        this.health = health;
        this.wave = 1;
        this.timerWait = 0;
    }
    return Game;
}());
//# sourceMappingURL=game.js.map

/***/ }),

/***/ 476:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__map_service__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__square_square__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__monster_monster__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__monster_monster_sound__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__square_square_enum__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__square_square_service__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__util_data_subject__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__util_data_subject_util__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__character_character_service__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__game_game_service__ = __webpack_require__(137);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var NUMBER_MONSTER = 10;
var NUMBER_SEC_BETWEEN_WAVE = 15;
var MapComponent = (function () {
    function MapComponent(mapService, dataSubjectUtil, squareService) {
        this.mapService = mapService;
        this.dataSubjectUtil = dataSubjectUtil;
        this.squareService = squareService;
        this.map = [];
        this.mapPath = [];
        this.listMonster = [];
        // Square subjet
        this.squareSubject = new __WEBPACK_IMPORTED_MODULE_3_rxjs__["Subject"];
        this.notifyParent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* EventEmitter */]();
        // this.listMonster.push(new Monster("1", 0, 0, 0, 0, 0));
        this.squareSize = 80;
        this.squarePadding = 6;
        this.cptMonster = 0;
    }
    MapComponent.prototype.ngOnInit = function () {
        this.handleParent();
        this.getMap();
    };
    /**
     * Trigger after render view
     */
    MapComponent.prototype.ngAfterViewInit = function () {
        // Get begin Square
        // this.listSquare.changes.subscribe(this.findBeginSquare.bind(this));
    };
    MapComponent.prototype.notifyChildren = function (event) {
        this.squareSubject.next(event);
    };
    MapComponent.prototype.handleParent = function () {
        var _this = this;
        this.parentSubject.subscribe(function (event) {
            if (event.action === "endGame") {
                // Stop timer Observable
                _this.timer.unsubscribe();
            }
        });
    };
    /**
     * Init Monster with carcteristique of map
     */
    MapComponent.prototype.initMonster = function () {
        if (this.gameService.game.wave <= this.mapJSON.waveMonster.length) {
            var rotation = this.squareService.getDegreesNextSquare(this.beginSquareComponent.square.direction);
            var sizeMonster = 68;
            var startPosition = this.squareService.initStartPositionOfMonsterInSquare(this.beginSquareComponent.square.direction, sizeMonster);
            var currentWaveMonster = this.mapJSON.waveMonster[this.gameService.game.wave - 1];
            this.mapService.numberMonsterInWave = currentWaveMonster.listMonster.length;
            this.mapService.numberMonsterKillInWave = 0;
            for (var i = 0; i < currentWaveMonster.listMonster.length; i++) {
                var monsterJSON = currentWaveMonster.listMonster[i];
                var monster = new __WEBPACK_IMPORTED_MODULE_4__monster_monster__["a" /* Monster */](monsterJSON.type, 0, monsterJSON.health, 0, 0, 0);
                monster.size = sizeMonster;
                // Assign position
                monster[this.beginSquareComponent.square.directionProperties] = startPosition;
                monster.rotation = rotation;
                this.listMonster.push(monster);
            }
        }
    };
    /**
     * Get begin square component
     * @param  {[type]} event [description]
     * @return {[type]}       [description]
     */
    MapComponent.prototype.handleChildRenderSquareComponent = function (event) {
        if (event instanceof __WEBPACK_IMPORTED_MODULE_8__util_data_subject__["a" /* DataSubject */]) {
            // Initialisation game
            if (event.action === "initSquare") {
                this.initGame(event);
            }
            else if (event.action === "nextPath") {
                this.monsterGoNextPath(event);
            }
            else if (event.action === "damageMonster") {
                this.notifyParent.emit(this.dataSubjectUtil.generateDataSubject("damageMonster", null, null));
                this.mapService.numberMonsterKillInWave++;
            }
            else if (event.action === "checkCharacterAttachMonster") {
                // Notify parent for check character to attach this monster
                this.notifyChildren(this.dataSubjectUtil.generateDataSubject("checkCharacterAttachMonster", null, event.condition));
            }
            else if (event.action === "attackMonster") {
                this.notifyChildren(this.dataSubjectUtil.generateDataSubject("attackMonster", event.data, event.condition));
            }
        }
    };
    /**
     * Assign monster to nearby character
     * @param  {Square}  square  [description]
     * @param  {Monster} monster [description]
     */
    MapComponent.prototype.assignMonsterToNearbyCharacter = function (square, monster) {
        this.notifyChildren(this.dataSubjectUtil.generateDataSubject("assignMonsterToNearbyCharacter", monster, square));
    };
    /**
     * Handle of initialisation of map
     * @param  {DataSubject} event [description]
     * @return {[type]}                [description]
     */
    MapComponent.prototype.initGame = function (event) {
        // Attach next square path if is square path
        if (event.data.square.type === __WEBPACK_IMPORTED_MODULE_6__square_square_enum__["a" /* SquareEnum */].TYPE_PATH || event.data.square.type === __WEBPACK_IMPORTED_MODULE_6__square_square_enum__["a" /* SquareEnum */].TYPE_BEGIN) {
            var nextSquare = this.findNextSquarePath(event.data.square);
            // nextSquare.direction =
            event.data.nextSquare = nextSquare;
        }
        // On start get begin square
        if (event.data.square.type === __WEBPACK_IMPORTED_MODULE_6__square_square_enum__["a" /* SquareEnum */].TYPE_BEGIN) {
            this.onStartGetBeginSquare(event.data);
        }
        // If last square, it start game
        if (event.data.square.x === this.lastSquare.x
            && event.data.square.y === this.lastSquare.y) {
            this.startGame();
        }
    };
    MapComponent.prototype.startGame = function () {
        var _this = this;
        this.gameStart = true;
        var timer = __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].timer(0, 40);
        this.timer = timer.subscribe(function () {
            _this.eventGame();
        });
    };
    MapComponent.prototype.onStartGetBeginSquare = function (beginSquare) {
        this.beginSquareComponent = beginSquare;
        var direction = this.squareService.getDirectionNextSquare(this.beginSquareComponent.square, this.beginSquareComponent.nextSquare);
        var directionProperties = this.squareService.convertDirectionForPropretiesCSSUse(direction);
        // Assign direction
        this.beginSquareComponent.square.direction = direction;
        this.beginSquareComponent.square.nextDirection = direction;
        this.beginSquareComponent.square.directionProperties = directionProperties;
        // Init monsters to begin square
        this.initMonster();
        // Launch timer to attach list monster
        this.launchWaveMonster();
    };
    MapComponent.prototype.launchWaveMonster = function () {
        var _this = this;
        var timer = __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].timer(0, this.mapJSON.waveMonster[this.gameService.game.wave - 1].timer);
        this.timerMonster = timer.subscribe(function () {
            _this.pushMonsterInBeginSquare();
        });
    };
    MapComponent.prototype.monsterGoNextPath = function (event) {
        // Get next square of current square - event.condition
        var nextSquare = this.findNextSquarePath(event.condition);
        // Get next square of next square for get direction to next square
        if (nextSquare !== null) {
            var nextSquareOfNextSquare = this.findNextSquarePath(nextSquare);
            var monster = event.data;
            // Get last direction in DataSubject
            nextSquare.lastDirection = event.condition.direction;
            // Assign direction
            var direction = event.condition.direction;
            if (nextSquareOfNextSquare !== null) {
                direction = this.squareService.getDirectionNextSquare(nextSquare, nextSquareOfNextSquare);
            }
            var directionProperties = this.squareService.convertDirectionForPropretiesCSSUse(direction);
            nextSquare.directionProperties = directionProperties;
            nextSquare.direction = direction;
            nextSquare.nextDirection = direction;
            // If square is angle
            if (nextSquare.type === __WEBPACK_IMPORTED_MODULE_6__square_square_enum__["a" /* SquareEnum */].TYPE_PATH && nextSquare.lastDirection !== nextSquare.direction) {
                nextSquare.directionProperties = this.squareService.convertDirectionForPropretiesCSSUse(nextSquare.lastDirection);
                // Start position of monster to next square
                monster[nextSquare.directionProperties] = this.squareService.initStartPositionOfMonsterInSquare(nextSquare.lastDirection, monster.size);
            }
            else {
                // Start position of monster to next square
                monster[nextSquare.directionProperties] = this.squareService.initStartPositionOfMonsterInSquare(direction, monster.size);
            }
            this.notifyChildren(this.dataSubjectUtil.generateDataSubject("assignMonster", monster, nextSquare));
            // Attach monster to nearby square
            this.assignMonsterToNearbyCharacter(nextSquare, monster);
        }
    };
    MapComponent.prototype.eventGame = function () {
        var _this = this;
        // If wave is end
        if (this.mapService.numberMonsterInWave === this.mapService.numberMonsterKillInWave) {
            // Stop timer Observable
            this.timer.unsubscribe();
            this.gameService.game.wave++;
            if (this.gameService.game.wave <= this.mapJSON.waveMonster.length) {
                this.initMonster();
                this.gameStart = false;
                var timer = __WEBPACK_IMPORTED_MODULE_3_rxjs__["Observable"].timer(0, 1000);
                this.gameService.game.timerWait = NUMBER_SEC_BETWEEN_WAVE;
                this.timer = timer.subscribe(function () {
                    _this.gameService.game.timerWait--;
                    if (_this.gameService.game.timerWait === 0) {
                        _this.timer.unsubscribe();
                        _this.startGame();
                        _this.launchWaveMonster();
                    }
                });
            }
            else {
                this.notifyParent.emit(this.dataSubjectUtil.generateDataSubject("successGame", null, null));
            }
        }
        else {
            this.notifyChildren(this.dataSubjectUtil.generateDataSubject("timeEvent", null, null));
        }
    };
    MapComponent.prototype.initMap = function () {
        var _this = this;
        this.map = [];
        // Initialisation square of monster path
        this.initMapPath();
        if (this.mapJSON !== {}) {
            this.mapJSON["cases"].forEach(function (squareLine, i) {
                var tabSquare = [];
                if (squareLine !== []) {
                    tabSquare = _this.initLineSquare(squareLine);
                }
                if (tabSquare !== []) {
                    _this.map.push(tabSquare);
                }
            });
            this.lastSquare = this.getLastSquare();
        }
    };
    /**
     * Initialisation of line square
     * @param  {any}    squareLine [description]
     * @return {[type]}            [description]
     */
    MapComponent.prototype.initLineSquare = function (squareLine) {
        var _this = this;
        var tabSquare = [];
        squareLine.forEach(function (square, i) {
            var squareComponent = null;
            squareComponent = new __WEBPACK_IMPORTED_MODULE_2__square_square__["a" /* Square */](square.type, square.x + 1, square.y + 1, _this.squareSize, _this.squarePadding, square.cssClass);
            tabSquare.push(squareComponent);
        });
        return tabSquare;
    };
    /**
     * Initialisation square of monster path
     * @return {[type]} [description]
     */
    MapComponent.prototype.initMapPath = function () {
        var _this = this;
        var mapPath = [];
        if (this.mapJSON !== {}) {
            this.mapJSON["monsterPath"].forEach(function (square, i) {
                mapPath.push(new __WEBPACK_IMPORTED_MODULE_2__square_square__["a" /* Square */](square.type, square.x + 1, square.y + 1, _this.squareSize, _this.squarePadding, square.cssClass));
            });
        }
        this.mapPath = mapPath;
    };
    /**
     * Get last square of map
     * @return {Square} last square
     */
    MapComponent.prototype.getLastSquare = function () {
        var cases = this.mapJSON["cases"];
        var lastLineSquare = cases[cases.length - 1];
        var lastSquare = lastLineSquare[lastLineSquare.length - 1];
        return lastSquare;
    };
    /**
     * Check if square is path
     * @param  {any}    square [description]
     * @return {[type]}        [description]
     */
    MapComponent.prototype.isSquarePathExist = function (square) {
        var response = false;
        if (square !== {} && square.type === __WEBPACK_IMPORTED_MODULE_6__square_square_enum__["a" /* SquareEnum */].TYPE_PATH && this.mapPath[square.x + "_" + square.y] !== null) {
            response = true;
        }
        return response;
    };
    MapComponent.prototype.getMap = function () {
        this.mapJSON = {
            "level": "2",
            "cases": [
                [
                    {
                        "x": 0,
                        "y": 0,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 1,
                        "y": 0,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 2,
                        "y": 0,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 3,
                        "y": 0,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 4,
                        "y": 0,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 5,
                        "y": 0,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 6,
                        "y": 0,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 7,
                        "y": 0,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 8,
                        "y": 0,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 9,
                        "y": 0,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    }
                ],
                [
                    {
                        "x": 0,
                        "y": 1,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 1,
                        "y": 1,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 2,
                        "y": 1,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 3,
                        "y": 1,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 4,
                        "y": 1,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 5,
                        "y": 1,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 6,
                        "y": 1,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 7,
                        "y": 1,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 8,
                        "y": 1,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 9,
                        "y": 1,
                        "type": "begin",
                        "cssClass": "texture path-1"
                    }
                ],
                [
                    {
                        "x": 0,
                        "y": 2,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 1,
                        "y": 2,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 2,
                        "y": 2,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 3,
                        "y": 2,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 4,
                        "y": 2,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 5,
                        "y": 2,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 6,
                        "y": 2,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 7,
                        "y": 2,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 8,
                        "y": 2,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 9,
                        "y": 2,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    }
                ],
                [
                    {
                        "x": 0,
                        "y": 3,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 1,
                        "y": 3,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 2,
                        "y": 3,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 3,
                        "y": 3,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 4,
                        "y": 3,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 5,
                        "y": 3,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 6,
                        "y": 3,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 7,
                        "y": 3,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 8,
                        "y": 3,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 9,
                        "y": 3,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    }
                ],
                [
                    {
                        "x": 0,
                        "y": 4,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 1,
                        "y": 4,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 2,
                        "y": 4,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 3,
                        "y": 4,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 4,
                        "y": 4,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 5,
                        "y": 4,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 6,
                        "y": 4,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 7,
                        "y": 4,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 8,
                        "y": 4,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 9,
                        "y": 4,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    }
                ],
                [
                    {
                        "x": 0,
                        "y": 5,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 1,
                        "y": 5,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 2,
                        "y": 5,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 3,
                        "y": 5,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 4,
                        "y": 5,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 5,
                        "y": 5,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 6,
                        "y": 5,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 7,
                        "y": 5,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    },
                    {
                        "x": 8,
                        "y": 5,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 9,
                        "y": 5,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    }
                ],
                [
                    {
                        "x": 0,
                        "y": 6,
                        "type": "end",
                        "cssClass": "texture path-1"
                    },
                    {
                        "x": 1,
                        "y": 6,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 2,
                        "y": 6,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 3,
                        "y": 6,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 4,
                        "y": 6,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 5,
                        "y": 6,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 6,
                        "y": 6,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 7,
                        "y": 6,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 8,
                        "y": 6,
                        "type": "path",
                        "cssClass": "texture path-2"
                    },
                    {
                        "x": 9,
                        "y": 6,
                        "type": "stone",
                        "cssClass": "texture grass-1"
                    }
                ]
            ],
            "monsterPath": [
                {
                    "x": 9,
                    "y": 1,
                    "type": "begin",
                    "cssClass": "texture path-1"
                },
                {
                    "x": 8,
                    "y": 1,
                    "type": "path"
                },
                {
                    "x": 7,
                    "y": 1,
                    "type": "path"
                },
                {
                    "x": 6,
                    "y": 1,
                    "type": "path"
                },
                {
                    "x": 5,
                    "y": 1,
                    "type": "path"
                },
                {
                    "x": 4,
                    "y": 1,
                    "type": "path"
                },
                {
                    "x": 3,
                    "y": 1,
                    "type": "path"
                },
                {
                    "x": 2,
                    "y": 1,
                    "type": "path"
                },
                {
                    "x": 2,
                    "y": 2,
                    "type": "path"
                },
                {
                    "x": 2,
                    "y": 3,
                    "type": "path"
                },
                {
                    "x": 2,
                    "y": 4,
                    "type": "path"
                },
                {
                    "x": 3,
                    "y": 4,
                    "type": "path"
                },
                {
                    "x": 4,
                    "y": 4,
                    "type": "path"
                },
                {
                    "x": 5,
                    "y": 4,
                    "type": "path"
                },
                {
                    "x": 6,
                    "y": 4,
                    "type": "path"
                },
                {
                    "x": 7,
                    "y": 4,
                    "type": "path"
                },
                {
                    "x": 8,
                    "y": 4,
                    "type": "path"
                },
                {
                    "x": 8,
                    "y": 5,
                    "type": "path"
                },
                {
                    "x": 8,
                    "y": 6,
                    "type": "path"
                },
                {
                    "x": 7,
                    "y": 6,
                    "type": "path"
                },
                {
                    "x": 6,
                    "y": 6,
                    "type": "path"
                },
                {
                    "x": 5,
                    "y": 6,
                    "type": "path"
                },
                {
                    "x": 4,
                    "y": 6,
                    "type": "path"
                },
                {
                    "x": 3,
                    "y": 6,
                    "type": "path"
                },
                {
                    "x": 2,
                    "y": 6,
                    "type": "path"
                },
                {
                    "x": 1,
                    "y": 6,
                    "type": "path"
                },
                {
                    "x": 0,
                    "y": 6,
                    "type": "end"
                }
            ],
            "waveMonster": [
                {
                    "timer": 2500,
                    "listMonster": [
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        }
                    ]
                },
                {
                    "timer": 2000,
                    "listMonster": [
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        }
                    ]
                },
                {
                    "timer": 2000,
                    "listMonster": [
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "2",
                            "health": 100
                        },
                        {
                            "type": "2",
                            "health": 100
                        },
                        {
                            "type": "1",
                            "health": 50
                        }
                    ]
                },
                {
                    "timer": 2000,
                    "listMonster": [
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "2",
                            "health": 100
                        },
                        {
                            "type": "2",
                            "health": 100
                        },
                        {
                            "type": "2",
                            "health": 100
                        },
                        {
                            "type": "2",
                            "health": 100
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        },
                        {
                            "type": "1",
                            "health": 50
                        }
                    ]
                }
            ]
        };
        this.initMap();
    };
    /**
     * Find next square path
     * @param  {Square} currentSquare [description]
     * @return {Square} next square
     */
    MapComponent.prototype.findNextSquarePath = function (currentSquare) {
        var indexSquare = this.mapPath.findIndex(function (square) { return square.x === currentSquare.x
            && square.y === currentSquare.y; });
        var nextSquare = null;
        // If next square exist, get this
        if (indexSquare + 1 !== this.mapPath.length) {
            nextSquare = this.mapPath[indexSquare + 1];
        }
        return nextSquare;
    };
    /**
     * Assign begin square to squareBegin
     * @param  {QueryList <SquareComponent>} listSquare <SquareComponent>
     */
    MapComponent.prototype.findBeginSquare = function (listSquare) {
        this.beginSquareComponent = listSquare.filter(function (square) { return square.square.type === __WEBPACK_IMPORTED_MODULE_6__square_square_enum__["a" /* SquareEnum */].TYPE_BEGIN; })[0];
        this.beginSquareComponent.attachMonster(this.listMonster[0]);
    };
    /**
     * Remove monster
     * @param  {Monster} monster [description]
     * @return {[type]}          [description]
     */
    MapComponent.prototype.removeMonster = function (monster) {
        var index = this.listMonster.indexOf(monster);
        this.listMonster.splice(index);
    };
    /**
     * Push monster to begin square and stop if all attached
     */
    MapComponent.prototype.pushMonsterInBeginSquare = function () {
        // Push monster in begin square
        var monster = this.listMonster[this.cptMonster];
        this.beginSquareComponent.attachMonster(monster);
        // this.removeMonster(monster);
        this.cptMonster++;
        // If all monster is attached, stop timer
        if (this.cptMonster === this.listMonster.length) {
            this.timerMonster.unsubscribe();
        }
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3_rxjs__["Subject"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3_rxjs__["Subject"]) === 'function' && _a) || Object)
    ], MapComponent.prototype, "parentSubject", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Output */])(), 
        __metadata('design:type', Object)
    ], MapComponent.prototype, "notifyParent", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])(), 
        __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_11__game_game_service__["a" /* GameService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_11__game_game_service__["a" /* GameService */]) === 'function' && _b) || Object)
    ], MapComponent.prototype, "gameService", void 0);
    MapComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'app-map',
            template: __webpack_require__(547),
            styles: [__webpack_require__(537)],
            providers: [__WEBPACK_IMPORTED_MODULE_9__util_data_subject_util__["a" /* DataSubjectUtil */], __WEBPACK_IMPORTED_MODULE_7__square_square_service__["a" /* SquareService */], __WEBPACK_IMPORTED_MODULE_10__character_character_service__["a" /* CharacterService */], __WEBPACK_IMPORTED_MODULE_5__monster_monster_sound__["a" /* MonsterSound */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__map_service__["a" /* MapService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__map_service__["a" /* MapService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_9__util_data_subject_util__["a" /* DataSubjectUtil */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_9__util_data_subject_util__["a" /* DataSubjectUtil */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_7__square_square_service__["a" /* SquareService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_7__square_square_service__["a" /* SquareService */]) === 'function' && _e) || Object])
    ], MapComponent);
    return MapComponent;
    var _a, _b, _c, _d, _e;
}());
//# sourceMappingURL=map.component.js.map

/***/ }),

/***/ 477:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__monster__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__util_data_subject_util__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__square_square_service__ = __webpack_require__(139);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MonsterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MonsterComponent = (function () {
    function MonsterComponent(dataSubjectUtil, squareService) {
        this.dataSubjectUtil = dataSubjectUtil;
        this.squareService = squareService;
        this.notifyParent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* EventEmitter */]();
    }
    MonsterComponent.prototype.ngOnInit = function () {
        this.handleParent();
    };
    MonsterComponent.prototype.ngOnDestroy = function () {
        this.parentSubject.unsubscribe();
    };
    MonsterComponent.prototype.handleParent = function () {
        var _this = this;
        this.parentSubject.subscribe(function (event) {
            if (event.action === "walkMonster") {
                // console.log("walkMonster :", event.condition.directionProperties)
                // Change properties left or top to monster in square
                _this.monster[event.condition.directionProperties] = _this.squareService.convertSizeDirection(event.condition.direction, _this.monster[event.condition.directionProperties]);
                // console.log("left: ", this.monster.left, "top :", this.monster.top)
                // Increment distance of monster in square
                _this.monster.distance = _this.monster.distance + _this.squareService.STEP;
                if (_this.parentSubject.observers.length > 0) {
                    // Notify parent monster has changed for controll this
                    _this.notifyParent.next(_this.dataSubjectUtil.generateDataSubject("controlMonster", _this, null));
                }
            }
        });
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__monster__["a" /* Monster */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__monster__["a" /* Monster */]) === 'function' && _a) || Object)
    ], MonsterComponent.prototype, "monster", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Output */])(), 
        __metadata('design:type', Object)
    ], MonsterComponent.prototype, "notifyParent", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])(), 
        __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_rxjs__["Subject"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2_rxjs__["Subject"]) === 'function' && _b) || Object)
    ], MonsterComponent.prototype, "parentSubject", void 0);
    MonsterComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'app-monster',
            template: __webpack_require__(548),
            styles: [__webpack_require__(538)]
        }), 
        __metadata('design:paramtypes', [(typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__util_data_subject_util__["a" /* DataSubjectUtil */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__util_data_subject_util__["a" /* DataSubjectUtil */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__square_square_service__["a" /* SquareService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__square_square_service__["a" /* SquareService */]) === 'function' && _d) || Object])
    ], MonsterComponent);
    return MonsterComponent;
    var _a, _b, _c, _d;
}());
//# sourceMappingURL=monster.component.js.map

/***/ }),

/***/ 478:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MonsterEnum; });
var MonsterEnum = {
    MONSTER_1: "1",
    MONSTER_2: "2",
    MONSTER_3: "3",
};
//# sourceMappingURL=monster.enum.js.map

/***/ }),

/***/ 479:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__character_character__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__character_fire__ = __webpack_require__(473);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__monster_monster_sound__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__square_square__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__util_data_subject__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__util_data_subject_util__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__square_enum__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__square_service__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__character_character_service__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__game_game_service__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__map_map_service__ = __webpack_require__(207);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SquareComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var DAMAGE_CHARACTER = 20;
var SquareComponent = (function () {
    function SquareComponent(dataSubjectUtil, squareService, characterService, soundMonster) {
        this.dataSubjectUtil = dataSubjectUtil;
        this.squareService = squareService;
        this.characterService = characterService;
        this.soundMonster = soundMonster;
        this.listMonster = [];
        this.characterSubject = new __WEBPACK_IMPORTED_MODULE_5_rxjs__["Subject"]();
        this.renderComponent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* EventEmitter */]();
        this.monsterSubject = new __WEBPACK_IMPORTED_MODULE_5_rxjs__["Subject"]();
        this.listFire = [];
        this.isExistCharacter = false;
        this.character = null;
        this.assignMonsterToCharacter = null;
    }
    SquareComponent.prototype.ngOnInit = function () {
        // Define response to parent when I notify
        this.handleParent();
        // Notify map this square is init
        this.notifyParent(this.dataSubjectUtil.generateDataSubject("initSquare", this, null));
    };
    SquareComponent.prototype.ngOnDestroy = function () {
        this.parentSubject.unsubscribe();
    };
    SquareComponent.prototype.handleParent = function () {
        var _this = this;
        this.parentSubject.subscribe(function (event) {
            if (event.action === "timeEvent") {
                _this.walkMonster();
                _this.manageCharacter(event.action, null);
                _this.attackAssignMonster();
            }
            else if (event.action === "assignMonster" && event.condition.x === _this.square.x && event.condition.y === _this.square.y) {
                // Assign direction if empty
                _this.assignDirection(event.condition);
                _this.attachMonster(event.data);
            }
            else if (event.action === "checkCharacterAttachMonster" && _this.assignMonsterToCharacter !== null) {
                _this.manageCharacter(event.action, event.condition);
            }
            else if (event.action === "assignMonsterToNearbyCharacter" && _this.character !== null) {
                if (_this.squareService.isInPerimeterCharacter(_this.square, event.condition) && _this.assignMonsterToCharacter === null) {
                    // Notify if monster is has proximity character
                    _this.assignMonsterToCharacter = event.data;
                    _this.notifyCharacterComponent(_this.dataSubjectUtil.generateDataSubject("attachMonster", event.data, null));
                }
            }
            else if (event.action === "attackMonster") {
                _this.attackThisMonster(event);
            }
        });
    };
    /**
     * Assign last and new direction of next path if they are empty
     * @param  {Square} square [description]
     * @return {[type]}        [description]
     */
    SquareComponent.prototype.assignDirection = function (square) {
        if (this.square.lastDirection === null) {
            this.square.lastDirection = square.lastDirection;
            this.square.direction = square.direction;
            this.square.nextDirection = square.nextDirection;
            this.square.directionProperties = square.directionProperties;
        }
    };
    SquareComponent.prototype.initCharacter = function () {
        if (this.character === null && this.isSpawnCharacter()) {
            var character = new __WEBPACK_IMPORTED_MODULE_1__character_character__["a" /* Character */]("mini", 1, 50, 0, 10, 200, false, null, null);
            var cost = this.characterService.buyCharacter(character);
            // If user has enough money
            if (this.gameService.game.money >= cost) {
                this.gameService.game.money = this.gameService.game.money - cost;
                this.character = character;
                this.character.cptWaitAttack = 20;
                this.characterSubject = new __WEBPACK_IMPORTED_MODULE_5_rxjs__["Subject"]();
            }
        }
    };
    SquareComponent.prototype.addCharacter = function () {
        if (this.character !== null) {
            this.notifyCharacterComponent(this.dataSubjectUtil.generateDataSubject("addLevel", null, null));
        }
    };
    SquareComponent.prototype.manageCharacter = function (action, monster) {
        if (this.character !== null) {
            // Manage direction and attack of character
            if (action === "timeEvent") {
                this.notifyCharacterComponent(this.dataSubjectUtil.generateDataSubject("manageCharacter", this.square, null));
                this.checkAssignMonsterIsDead();
            }
            else if (action === "checkCharacterAttachMonster") {
                // If monster assign is out perimeter, detach this
                if (monster.id === this.assignMonsterToCharacter.id) {
                    var isProximity = this.squareService.isInPerimeterCharacter(this.square, this.assignMonsterToCharacter.square);
                    if (!isProximity || (this.assignMonsterToCharacter.square.type === __WEBPACK_IMPORTED_MODULE_8__square_enum__["a" /* SquareEnum */].TYPE_END && isProximity)) {
                        this.assignMonsterToCharacter = null;
                        this.notifyCharacterComponent(this.dataSubjectUtil.generateDataSubject("detachMonster", null, null));
                    }
                }
            }
        }
    };
    /**
     * Check if assign monster is dead
     */
    SquareComponent.prototype.checkAssignMonsterIsDead = function () {
        if (this.assignMonsterToCharacter !== null && this.assignMonsterToCharacter.health <= 0) {
            this.assignMonsterToCharacter = null;
        }
    };
    /**
     * Check if square is spawn for character
     * @return {boolean} true is spawn
     */
    SquareComponent.prototype.isSpawnCharacter = function () {
        var response = false;
        if (this.square.type !== __WEBPACK_IMPORTED_MODULE_8__square_enum__["a" /* SquareEnum */].TYPE_END && this.square.type !== __WEBPACK_IMPORTED_MODULE_8__square_enum__["a" /* SquareEnum */].TYPE_BEGIN
            && this.square.type !== __WEBPACK_IMPORTED_MODULE_8__square_enum__["a" /* SquareEnum */].TYPE_PATH) {
            response = true;
        }
        return response;
    };
    /**
     * Attach Monster to this square
     * @param  {Monster} monster
     */
    SquareComponent.prototype.attachMonster = function (monster) {
        monster.square = this.square;
        this.listMonster.push(monster);
        // Update monster subject
        this.monsterSubject = new __WEBPACK_IMPORTED_MODULE_5_rxjs__["Subject"]();
        // Notify parent for check character to attach this monster
        this.notifyParent(this.dataSubjectUtil.generateDataSubject("checkCharacterAttachMonster", null, monster));
    };
    /**
     * Remove monster
     * @param  {Monster} monster [description]
     * @return {[type]}          [description]
     */
    SquareComponent.prototype.removeMonster = function (monster) {
        var index = this.listMonster.indexOf(monster);
        this.listMonster.splice(index, 1);
    };
    SquareComponent.prototype.walkMonster = function () {
        if (this.listMonster.length > 0) {
            this.notifyMonsterComponent(this.dataSubjectUtil.generateDataSubject("walkMonster", this.nextSquare, this.square));
        }
    };
    /**
     * Character attack assign monster
     */
    SquareComponent.prototype.attackAssignMonster = function () {
        if (this.character !== null && this.assignMonsterToCharacter !== null) {
            this.character.cptWaitAttack--;
            if (this.character.cptWaitAttack === 0) {
                this.notifyParent(this.dataSubjectUtil.generateDataSubject("attackMonster", this.character, this.assignMonsterToCharacter));
                this.character.cptWaitAttack = 25;
            }
        }
    };
    SquareComponent.prototype.attackThisMonster = function (event) {
        var _this = this;
        if (this.listMonster.length > 0) {
            var findMonster = this.listMonster.find(function (monster) { return monster.id === event.condition.id; });
            if (typeof findMonster !== "undefined") {
                this.soundMonster.soundHurt();
                findMonster.health = findMonster.health - DAMAGE_CHARACTER;
                // If monster is dead - earn money
                if (findMonster.health <= 0) {
                    this.removeMonster(findMonster);
                    this.gameService.earnMoney(findMonster);
                    this.mapService.numberMonsterKillInWave++;
                }
                // Add animate fire in square
                this.listFire.push(new __WEBPACK_IMPORTED_MODULE_2__character_fire__["a" /* Fire */](30));
                // Delete fire to end animation
                setTimeout(function () {
                    _this.listFire.splice(0);
                }, 800);
            }
        }
    };
    SquareComponent.prototype.notifyMonsterComponent = function (event) {
        this.monsterSubject.next(event);
    };
    SquareComponent.prototype.notifyCharacterComponent = function (event) {
        this.characterSubject.next(event);
    };
    SquareComponent.prototype.handleMonsterComponent = function (event) {
        if (event instanceof __WEBPACK_IMPORTED_MODULE_6__util_data_subject__["a" /* DataSubject */]) {
            // Control to monster component
            if (event.action === "controlMonster") {
                this.controlMonster(event);
            }
        }
    };
    SquareComponent.prototype.damageMonster = function (monster) {
        this.notifyParent(this.dataSubjectUtil.generateDataSubject("damageMonster", null, null));
        // Notify parent for check character to attach this monster
        this.notifyParent(this.dataSubjectUtil.generateDataSubject("checkCharacterAttachMonster", null, monster));
    };
    SquareComponent.prototype.controlMonster = function (event) {
        var monster = event.data.monster;
        if (monster.distance >= this.square.size) {
            monster[this.square.directionProperties] = null;
            monster["rotation"] = this.squareService.getDegreesNextSquare(this.square.direction);
            monster.distance = 0;
            // If next path exist
            if (this.square.type !== __WEBPACK_IMPORTED_MODULE_8__square_enum__["a" /* SquareEnum */].TYPE_END) {
                this.renderComponent.emit(this.dataSubjectUtil.generateDataSubject("nextPath", monster, this.square));
            }
            else {
                this.damageMonster(monster);
            }
            this.removeMonster(monster);
        }
        else if (this.square.type === __WEBPACK_IMPORTED_MODULE_8__square_enum__["a" /* SquareEnum */].TYPE_PATH && this.square.lastDirection !== null
            && this.square.lastDirection !== this.square.nextDirection) {
            var sizeIntervall = monster.size + this.square.padding;
            // If monster is in first part of square (size monster + padding)
            if (monster.distance <= sizeIntervall && this.square.lastDirection !== this.square.direction) {
                monster = this.changeDirectionInAngleSquare(this.square.lastDirection, monster);
            }
            else if (monster.distance > sizeIntervall && this.square.lastDirection === this.square.direction) {
                monster = this.changeDirectionInAngleSquare(this.square.nextDirection, monster);
            }
        }
    };
    /**
     * Change direction of monster in angle square
     * @param  {string}  direction to follow
     * @param  {Monster} monster   [description]
     * @return {Monster}            [description]
     */
    SquareComponent.prototype.changeDirectionInAngleSquare = function (direction, monster) {
        var newDirection = this.squareService.convertDirectionForPropretiesCSSUse(direction);
        this.square.directionProperties = newDirection;
        this.square.direction = direction;
        monster.rotation = this.squareService.getDegreesNextSquare(direction);
        return monster;
    };
    SquareComponent.prototype.notifyParent = function (event) {
        this.renderComponent.emit(event);
    };
    SquareComponent.prototype.searchMonsterProximity = function () {
        this.notifyParent(this.dataSubjectUtil.generateDataSubject("monsterNearby", null, this.square));
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__square_square__["a" /* Square */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__square_square__["a" /* Square */]) === 'function' && _a) || Object)
    ], SquareComponent.prototype, "square", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Output */])(), 
        __metadata('design:type', Object)
    ], SquareComponent.prototype, "renderComponent", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])(), 
        __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5_rxjs__["Subject"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5_rxjs__["Subject"]) === 'function' && _b) || Object)
    ], SquareComponent.prototype, "parentSubject", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])(), 
        __metadata('design:type', (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_11__game_game_service__["a" /* GameService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_11__game_game_service__["a" /* GameService */]) === 'function' && _c) || Object)
    ], SquareComponent.prototype, "gameService", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])(), 
        __metadata('design:type', (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_12__map_map_service__["a" /* MapService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_12__map_map_service__["a" /* MapService */]) === 'function' && _d) || Object)
    ], SquareComponent.prototype, "mapService", void 0);
    SquareComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'app-square',
            template: __webpack_require__(549),
            styles: [__webpack_require__(539)],
            providers: [__WEBPACK_IMPORTED_MODULE_7__util_data_subject_util__["a" /* DataSubjectUtil */], __WEBPACK_IMPORTED_MODULE_9__square_service__["a" /* SquareService */], __WEBPACK_IMPORTED_MODULE_10__character_character_service__["a" /* CharacterService */]],
        }), 
        __metadata('design:paramtypes', [(typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_7__util_data_subject_util__["a" /* DataSubjectUtil */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_7__util_data_subject_util__["a" /* DataSubjectUtil */]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_9__square_service__["a" /* SquareService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_9__square_service__["a" /* SquareService */]) === 'function' && _f) || Object, (typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_10__character_character_service__["a" /* CharacterService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_10__character_character_service__["a" /* CharacterService */]) === 'function' && _g) || Object, (typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_3__monster_monster_sound__["a" /* MonsterSound */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__monster_monster_sound__["a" /* MonsterSound */]) === 'function' && _h) || Object])
    ], SquareComponent);
    return SquareComponent;
    var _a, _b, _c, _d, _e, _f, _g, _h;
}());
//# sourceMappingURL=square.component.js.map

/***/ }),

/***/ 534:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(37)();
// imports


// module
exports.push([module.i, "nav {\n    background-color: #2d2d2d;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 535:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(37)();
// imports


// module
exports.push([module.i, ".character {\n  width: 68px;\n  height: 68px;\n  background-image: url(" + __webpack_require__(826) + ");\n}\n\n.character.hidden {\n  background: none;\n}\n\n.character-mini-1{\n  background-position: 0 0;\n}\n\n.character-mini-2{\n  background-position: 0 -68px;\n}\n\n.character-mini-3{\n  background-position: 0 -136px;\n}\n\n.character-medium-1{\n  background-position: -68px 0;\n}\n\n.character-medium-2{\n  background-position: -68px -68px;\n}\n\n.character-medium-3{\n  background-position: -68px -136px;\n}\n\n.character-big-1{\n  background-position: -136px 0;\n}\n\n.character-big-2{\n  background-position: -136px -68px;\n}\n\n.character-big-3{\n  background-position: -136px -136px;\n}\n\n.character-mega-1{\n  background-position: -204px 0;\n}\n\n.character-mega-2{\n  background-position: -204px -68px;\n}\n\n.character-maga-3{\n  background-position: -204px -136px;\n}\n\n.character-ultra-1{\n  background-position: -282px 0;\n}\n\n.character-ultra-2{\n  background-position: -282px -68px;\n}\n\n.character-ultra-3{\n  background-position: -282px -136px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 536:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(37)();
// imports


// module
exports.push([module.i, "div.state-user {\n    margin-top: 20px;\n    margin-left: 0;\n    margin-bottom: 0px;\n}\n\ndiv.state-user > div {\n    background-color: #2d2d2d;\n    height: 150px;\n    padding-top: 12px;\n    margin-right: 10px;\n    max-width: 180px;\n}\n\ndiv.state-user > div p {\n    color: white;\n    font-family: 'Calibri';\n}\n\ndiv.state-user > div p.state-title {\n    text-align: center;\n    font-size: 21px;\n    text-transform: uppercase;\n    margin-top: 5px;\n    margin-bottom: 0px;\n}\n\ndiv.state-user > div p.state-value {\n    text-align: center;\n    font-size: 16px;\n    margin: 0;\n}\n\ndiv.state-user > div p.state-icon {\n    text-align: center;\n    margin: 0;\n}\n\ndiv.state-user > div span.icon {\n    font-size: 64px;\n    color: white;\n}\n\ndiv#content-map .message-game {\n    padding: 20px;\n    background-color: #2d2d2d;\n    margin-top: 20px;\n    position: relative;\n}\n\ndiv#content-map .message-game span.icon {\n    color: white;\n    font-size: 64px;\n    position: absolute;\n    left: 16%;\n    top: 13px;\n}\n\ndiv#content-map .message-game p {\n    text-align: center;\n    color:white;\n    font-size: 16px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 537:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(37)();
// imports


// module
exports.push([module.i, "ul {\n  margin: 0;\n  padding: 0;\n  font-size: 0;\n}\n\nul.lineSquare {\n    border-left: 10px solid #2d2d2d;\n}\n\nul.lineSquare app-square {\n    display: inline-block;\n    vertical-align: top;\n}\n\nul.lineSquare app-square:last-child {\n    border-right: 10px solid #2d2d2d;\n    overflow: hidden;\n}\n\nul.lineSquare:first-child app-square {\n    border-top: 10px solid #2d2d2d;\n}\n\nul.lineSquare:last-child app-square {\n    border-bottom: 10px solid #2d2d2d;\n}\n\n#mapContent {\n  min-width: 900px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 538:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(37)();
// imports


// module
exports.push([module.i, ".monster{\n\tbackground: url(" + __webpack_require__(827) + ") no-repeat;\n  width: 68px;\n\theight: 68px;\n\tposition: absolute;\n\t-webkit-transform: rotate(180deg);\n\t        transform: rotate(180deg);\n\tz-index: 100;\n}\n\n.monster-1{\n\tbackground-position: 0 0;\n}\n\n.monster-2{\n\tbackground-position: -68px 0;\n}\n\n.monster-3{\n\tbackground-position: -136px 0;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 539:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(37)();
// imports


// module
exports.push([module.i, "li.square {\n  font-size: 0;\n  width: 80px;\n  height: 80px;\n  display: inline-block;\n  /*border: 1px solid #b2b2b2;*/\n  padding: 6px;\n  position: relative;\n}\n\n\nli.square.stone {\n  background-color: #b2b2b2;\n}\n\nli.square.path {\n  background-color: green;\n}\n\nli.square.begin {\n  background-color: blue;\n}\n\nli.square.end {\n  background-color: red;\n}\n\nul.fire-monster li div {\n  position: absolute;\n  background: url(" + __webpack_require__(828) + ") left top no-repeat;\n  /*background-size: 50px 50px;*/\n  z-index: 200;\n  width:96px;\n  height:96px;\n  -webkit-animation: play 0.8s steps(10);\n          animation: play 0.8s steps(10);\n}\n\n@-webkit-keyframes play {\n   100% { background-position: -960px 0px; }\n}\n\n@keyframes play {\n   100% { background-position: -960px 0px; }\n}\n\n.texture{\n\tbackground: url(" + __webpack_require__(829) + ") no-repeat;\n}\n\n.grass-1{\n\tbackground-position: 0 0;\n\twidth: 80px;\n\theight: 80px;\n}\n\n.grass-2{\n\tbackground-position: -80px 0;\n\twidth: 80px;\n\theight: 80px;\n}\n\n.path-1{\n\tbackground-position: -160px 0;\n\twidth: 80px;\n\theight: 80px;\n}\n\n.path-2{\n\tbackground-position: -240px 0;\n\twidth: 80px;\n\theight: 80px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 544:
/***/ (function(module, exports) {

module.exports = "<nav>\n  <div class=\"nav-wrapper\">\n    <div class=\"container\">\n      <a href=\"#\" class=\"brand-logo\">{{title}}</a>\n      <ul id=\"nav-mobile\" class=\"right hide-on-med-and-down\">\n        <li><a href=\"http://localhost:4200/dashboard\">Home</a></li>\n        <li><a href=\"http://localhost:4200/\">Sign in</a></li>\n        <li><a href=\"http://localhost:4200/register\">Sign up</a></li>\n      </ul>\n    </div>\n  </div>\n</nav>\n<app-game></app-game>\n"

/***/ }),

/***/ 545:
/***/ (function(module, exports) {

module.exports = "<div class=\"character character-{{character.type}}-{{character.level}}\" [ngClass]=\"style\"\n  [style.transform]=\"'rotate('+character.rotation+'deg)'\">\n</div>\n"

/***/ }),

/***/ 546:
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row state-user\">\n      <div class=\"col s3 health\">\n          <p class=\"state-title\">Health</p>\n          <p class=\"state-value\">{{gameService.game.health}}</p>\n          <p class=\"state-icon\"><span class=\"icon fa fa-heartbeat\"></span></p>\n      </div>\n      <div class=\"col s3\">\n          <p class=\"state-title\">Money</p>\n          <p class=\"state-value\">{{gameService.game.money}}</p>\n          <p class=\"state-icon\"><span class=\"icon fa fa-money\"></span></p>\n      </div>\n      <div class=\"col s3\">\n          <p class=\"state-title\">Waves</p>\n          <p class=\"state-value\">{{gameService.game.wave}}</p>\n          <p class=\"state-icon\"><span class=\"icon fa fa-info-circle\"></span></p>\n      </div>\n      <div class=\"col s2\">\n          <div *ngIf=\"gameService.game.timerWait > 0\">\n              <p class=\"state-title\">Wait</p>\n              <p class=\"state-value\">{{gameService.game.timerWait}} sec</p>\n              <p class=\"state-icon\"><span class=\"icon fa fa-pause\"></span></p>\n          </div>\n          <div *ngIf=\"gameService.game.timerWait === 0\">\n              <p class=\"state-title\">Stop</p>\n              <p class=\"state-value\">&nbsp;</p>\n              <p class=\"state-icon\"><span class=\"icon fa fa-play\"></span></p>\n          </div>\n      </div>\n    </div>\n</div>\n<div id=\"content-map\" class=\"container\">\n    <app-map [gameService]=\"gameService\" [parentSubject]=\"mapSubject\" (notifyParent)=\"handleChildMap($event)\"\n        *ngIf=\"gameService.game.health > 0 && !successGame\">\n    </app-map>\n    <div *ngIf=\"gameService.game.health === 0 && !successGame\" class=\"game-over message-game\">\n      <span class=\"icon fa fa-thumbs-o-down\"></span>\n      <p>Vous êtes mort ! La vengeance est un plat qui se mange froid !</p>\n    </div>\n    <div *ngIf=\"successGame\" class=\"game-success message-game\">\n      <span class=\"icon fa fa-thumbs-o-up\"></span>\n      <p>Vous avez gagné ! Félicitation !</p>\n    </div>\n</div>\n"

/***/ }),

/***/ 547:
/***/ (function(module, exports) {

module.exports = "<div id=\"mapContent\">\n  <ul *ngFor=\"let line of map\" class=\"lineSquare\">\n    <app-square *ngFor=\"let col of line;\" [square]=\"col\" (renderComponent)=\"handleChildRenderSquareComponent($event)\"\n      [parentSubject]=\"squareSubject\" [gameService]=\"gameService\" [mapService]=\"mapService\">\n    </app-square>\n  </ul>\n</div>\n"

/***/ }),

/***/ 548:
/***/ (function(module, exports) {

module.exports = "<div class=\"monster monster-{{monster.type}}\" [style.left]=\"monster.left+'px'\"\n  [style.top]=\"monster.top+'px'\" [style.transform]=\"'rotate('+monster.rotation+'deg)'\">\n</div>\n"

/***/ }),

/***/ 549:
/***/ (function(module, exports) {

module.exports = "<li class=\"square {{square.type}} {{square.style}}\" (click)=\"initCharacter()\">\n  <app-character *ngIf=\"character !== null\" (click)=\"addCharacter()\" [character]=\"character\" [square]=\"square\"\n    [parentSubject]=\"characterSubject\" (notifyParent)=\"handleCharacterComponent($event)\"></app-character>\n  <ul class=\"square-monster\" *ngIf=\"listMonster.length > 0\">\n    <li *ngFor=\"let monster of listMonster\" >\n      <app-monster [monster]=\"monster\" (notifyParent)=\"handleMonsterComponent($event)\"\n        [parentSubject]=\"monsterSubject\">\n      </app-monster>\n    </li>\n  </ul>\n  <!-- fire monster -->\n  <ul class=\"fire-monster\" *ngIf=\"listFire.length > 0\">\n    <li *ngFor=\"let fire of listFire\">\n      <div></div>\n    </li>\n  </ul>\n</li>\n"

/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__data_subject__ = __webpack_require__(209);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataSubjectUtil; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DataSubjectUtil = (function () {
    function DataSubjectUtil() {
    }
    /**
     * Generate DataSubect for communication with many component
     * @param  {string} action    [description]
     * @param  {any}    data      [description]
     * @param  {any}    condition [description]
     * @return {DataSubject}
     */
    DataSubjectUtil.prototype.generateDataSubject = function (action, data, condition) {
        return new __WEBPACK_IMPORTED_MODULE_1__data_subject__["a" /* DataSubject */](action, data, condition);
    };
    DataSubjectUtil = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* Injectable */])(), 
        __metadata('design:paramtypes', [])
    ], DataSubjectUtil);
    return DataSubjectUtil;
}());
//# sourceMappingURL=data.subject.util.js.map

/***/ }),

/***/ 826:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "character.cbcddd19950ec226c554.png";

/***/ }),

/***/ 827:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "monsters.4e2ca2b0d63fa509bf88.png";

/***/ }),

/***/ 828:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "fire.5dacf2220d32e6fee265.png";

/***/ }),

/***/ 829:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "textures.2099a81bc008bd74ac41.png";

/***/ }),

/***/ 834:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(363);


/***/ })

},[834]);
//# sourceMappingURL=main.bundle.js.map
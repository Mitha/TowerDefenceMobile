import { Injectable } from '@angular/core';
import {Http, Response, RequestOptions, Headers} from "@angular/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";


@Injectable()
export class MapService {

  map = [];
  mapJSON: string;
  mapUrl: string;
  numberMonsterInWave: number;
  numberMonsterKillInWave: number;
  envName: string;

  constructor(private http: Http) {
    this.mapUrl = `${environment.map_url}`;
    this.envName = `${environment.envName}`;
  }

  ngOnInit() {
  }

  getMap(): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.mapUrl, options)
      .map(this.extractData)
      .catch(this.handleError)
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

}

import { Component, OnInit, ViewChildren, QueryList, AfterViewInit, Output, EventEmitter, Input} from '@angular/core';
import {MapService} from './map.service';
import {SquareComponent} from '../square/square.component';
import {Square} from '../square/square';
import {Http, Response, RequestOptions} from "@angular/http";
import {environment} from "../../environments/environment";
import {Subject, Observable} from "rxjs";
import {Monster} from '../monster/monster';
import {MonsterSound} from '../monster/monster.sound';
import {SquareEnum} from '../square/square.enum';
import {SquareService} from '../square/square.service';
import {DataSubject} from '../util/data.subject';
import {DataSubjectUtil} from '../util/data.subject.util';
import {CharacterService} from '../character/character.service';
import {GameService} from '../game/game.service';

const NUMBER_MONSTER = 10;
const NUMBER_SEC_BETWEEN_WAVE = 15;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
  providers: [DataSubjectUtil, SquareService, CharacterService, MonsterSound]
})
export class MapComponent implements OnInit {

  map = [];
  mapPath = [];
  mapJSON : any;
  errorMessage: string;
  listMonster = [];
  beginSquareComponent: SquareComponent;
  lastSquare: Square;
  gameStart: boolean;
  // Square subjet
  squareSubject:Subject<any> = new Subject
  // Map subject
  @Input() parentSubject:Subject<any>;
  @Output() notifyParent = new EventEmitter();

  squareSize: number;
  squarePadding: number;

  timer: any;
  timerMonster: any;
  // Index future monster to push
  cptMonster: number;
  @Input() gameService: GameService;

  constructor(private mapService : MapService, private dataSubjectUtil: DataSubjectUtil, private squareService: SquareService) {
    // this.listMonster.push(new Monster("1", 0, 0, 0, 0, 0));
    this.squareSize = 80;
    this.squarePadding = 6;
    this.cptMonster = 0;
  }

  ngOnInit() {
    this.handleParent();
    this.getMap();
  }

  /**
   * Trigger after render view
   */
  ngAfterViewInit() {
    // Get begin Square
    // this.listSquare.changes.subscribe(this.findBeginSquare.bind(this));
  }

  notifyChildren(event) {
    this.squareSubject.next(event);
  }

  handleParent() {
    this.parentSubject.subscribe(event => {
      if (event.action === "endGame") {
        // Stop timer Observable
        this.timer.unsubscribe();
      }
    });
  }

  /**
   * Init Monster with carcteristique of map
   */
  initMonster() {
    if (this.gameService.game.wave <= this.mapJSON.waveMonster.length) {
        let rotation = this.squareService.getDegreesNextSquare(this.beginSquareComponent.square.direction);
        let sizeMonster = 68;
        let startPosition = this.squareService.initStartPositionOfMonsterInSquare(this.beginSquareComponent.square.direction, sizeMonster);
        let currentWaveMonster = this.mapJSON.waveMonster[this.gameService.game.wave - 1];
        this.mapService.numberMonsterInWave = currentWaveMonster.listMonster.length;
        this.mapService.numberMonsterKillInWave = 0;
        for (let i = 0; i < currentWaveMonster.listMonster.length; i++) {
          let monsterJSON = currentWaveMonster.listMonster[i];
          let monster = new Monster(monsterJSON.type, 0, monsterJSON.health, 0, 0, 0);
          monster.size = sizeMonster;
          // Assign position
          monster[this.beginSquareComponent.square.directionProperties] = startPosition;
          monster.rotation = rotation;
          this.listMonster.push(monster);
        }
    }
  }

  /**
   * Get begin square component
   * @param  {[type]} event [description]
   * @return {[type]}       [description]
   */
  handleChildRenderSquareComponent(event: DataSubject) {
    if (event instanceof DataSubject) {
      // Initialisation game
      if (event.action === "initSquare") {
        this.initGame(event);
      }
      // If square is a path and monster must go next path
      else if (event.action === "nextPath") {
        this.monsterGoNextPath(event);
      }
      else if (event.action === "damageMonster") {
        this.notifyParent.emit(this.dataSubjectUtil.generateDataSubject("damageMonster", null, null));
        this.mapService.numberMonsterKillInWave++;
        // this.timer.unsubscribe();
      }
      else if (event.action === "checkCharacterAttachMonster") {
        // Notify parent for check character to attach this monster
        this.notifyChildren(this.dataSubjectUtil.generateDataSubject("checkCharacterAttachMonster", null, event.condition))
      }
      // Notify children for trigger attack monster
      else if (event.action === "attackMonster") {
        this.notifyChildren(this.dataSubjectUtil.generateDataSubject("attackMonster", event.data, event.condition))
      }
    }
  }

  /**
   * Assign monster to nearby character
   * @param  {Square}  square  [description]
   * @param  {Monster} monster [description]
   */
  assignMonsterToNearbyCharacter(square: Square, monster: Monster) {
    this.notifyChildren(this.dataSubjectUtil.generateDataSubject("assignMonsterToNearbyCharacter", monster, square));
  }

  /**
   * Handle of initialisation of map
   * @param  {DataSubject} event [description]
   * @return {[type]}                [description]
   */
  initGame(event: DataSubject) {
    // Attach next square path if is square path
    if (event.data.square.type === SquareEnum.TYPE_PATH || event.data.square.type === SquareEnum.TYPE_BEGIN) {
      let nextSquare = this.findNextSquarePath(event.data.square);
      // nextSquare.direction =
      event.data.nextSquare = nextSquare;
    }
    // On start get begin square
    if (event.data.square.type === SquareEnum.TYPE_BEGIN) {
      this.onStartGetBeginSquare(event.data);
    }
    // If last square, it start game
    if (event.data.square.x === this.lastSquare.x
      && event.data.square.y === this.lastSquare.y) {
      this.startGame();
    }
  }

  startGame() {
    this.gameStart = true;
    let timer = Observable.timer(0, 40);
    this.timer = timer.subscribe( () =>  {
      this.eventGame();
    })
  }

  onStartGetBeginSquare(beginSquare) {
    this.beginSquareComponent = beginSquare;
    let direction = this.squareService.getDirectionNextSquare(this.beginSquareComponent.square, this.beginSquareComponent.nextSquare)
    let directionProperties = this.squareService.convertDirectionForPropretiesCSSUse(direction);
    // Assign direction
    this.beginSquareComponent.square.direction = direction;
    this.beginSquareComponent.square.nextDirection = direction;
    this.beginSquareComponent.square.directionProperties = directionProperties;
    // Init monsters to begin square
    this.initMonster();
    // Launch timer to attach list monster
    this.launchWaveMonster();
  }

  launchWaveMonster() {
      let timer = Observable.timer(0, this.mapJSON.waveMonster[this.gameService.game.wave - 1].timer);
      this.timerMonster = timer.subscribe( () => {
        this.pushMonsterInBeginSquare();
      })
  }

  monsterGoNextPath(event: DataSubject) {
    // Get next square of current square - event.condition
    let nextSquare = this.findNextSquarePath(event.condition);
    // Get next square of next square for get direction to next square
    if (nextSquare !== null) {
      let nextSquareOfNextSquare = this.findNextSquarePath(nextSquare);
      let monster  = event.data;
      // Get last direction in DataSubject
      nextSquare.lastDirection = event.condition.direction;
      // Assign direction
      let direction = event.condition.direction;
      if (nextSquareOfNextSquare !== null) {
          direction = this.squareService.getDirectionNextSquare(nextSquare, nextSquareOfNextSquare);
      }
      let directionProperties = this.squareService.convertDirectionForPropretiesCSSUse(direction);
      nextSquare.directionProperties = directionProperties;
      nextSquare.direction = direction;
      nextSquare.nextDirection = direction;

      // If square is angle
      if (nextSquare.type === SquareEnum.TYPE_PATH && nextSquare.lastDirection !== nextSquare.direction) {
        nextSquare.directionProperties = this.squareService.convertDirectionForPropretiesCSSUse(nextSquare.lastDirection);
        // Start position of monster to next square
        monster[nextSquare.directionProperties] = this.squareService.initStartPositionOfMonsterInSquare(nextSquare.lastDirection, monster.size);
      } else {
        // Start position of monster to next square
        monster[nextSquare.directionProperties] = this.squareService.initStartPositionOfMonsterInSquare(direction, monster.size);
      }
      this.notifyChildren(this.dataSubjectUtil.generateDataSubject("assignMonster", monster, nextSquare))

      // Attach monster to nearby square
      this.assignMonsterToNearbyCharacter(nextSquare, monster);
    }
  }

  eventGame() {
    // If wave is end
    if (this.mapService.numberMonsterInWave === this.mapService.numberMonsterKillInWave) {
        // Stop timer Observable
        this.timer.unsubscribe();
        this.gameService.game.wave++;
        if (this.gameService.game.wave <= this.mapJSON.waveMonster.length) {
            this.initMonster();
            this.gameStart = false;
            let timer = Observable.timer(0, 1000);
            this.gameService.game.timerWait = NUMBER_SEC_BETWEEN_WAVE;
            this.timer = timer.subscribe( () => {
                this.gameService.game.timerWait--;
                if (this.gameService.game.timerWait === 0) {
                    this.timer.unsubscribe();
                    this.startGame();
                    this.launchWaveMonster();
                }
            })
        } else {
            this.notifyParent.emit(this.dataSubjectUtil.generateDataSubject("successGame", null, null));
        }
    } else {
        this.notifyChildren(this.dataSubjectUtil.generateDataSubject("timeEvent", null, null));
    }
  }

  initMap() {
    this.map = [];
    // Initialisation square of monster path
    this.initMapPath();
    if (this.mapJSON !== {}) {
      this.mapJSON["cases"].forEach( (squareLine, i) => {

        let tabSquare = [];
        if (squareLine !== []) {
          tabSquare = this.initLineSquare(squareLine);
        }

        if (tabSquare !== []) {
          this.map.push(tabSquare);
        }

      })
      this.lastSquare = this.getLastSquare();
    }
  }

  /**
   * Initialisation of line square
   * @param  {any}    squareLine [description]
   * @return {[type]}            [description]
   */
  initLineSquare(squareLine: any) {
    let tabSquare = [];
    squareLine.forEach( (square, i) => {
      let squareComponent = null;
      squareComponent = new Square(square.type, square.x + 1, square.y + 1, this.squareSize, this.squarePadding, square.cssClass);
      tabSquare.push(squareComponent);
    });
    return tabSquare;
  }

  /**
   * Initialisation square of monster path
   * @return {[type]} [description]
   */
  initMapPath() {
    let mapPath = []
    if (this.mapJSON !== {}) {
      this.mapJSON["monsterPath"].forEach( (square, i) => {
        mapPath.push(new Square(square.type, square.x + 1, square.y + 1, this.squareSize, this.squarePadding, square.cssClass));
      })
    }
    this.mapPath = mapPath;
  }

  /**
   * Get last square of map
   * @return {Square} last square
   */
  getLastSquare() {
    var cases = this.mapJSON["cases"];
    var lastLineSquare = cases[cases.length - 1];
    var lastSquare = lastLineSquare[lastLineSquare.length - 1];
    return lastSquare;
  }

  /**
   * Check if square is path
   * @param  {any}    square [description]
   * @return {[type]}        [description]
   */
  isSquarePathExist(square: any) {
    let response = false;
    if (square !== {} && square.type === SquareEnum.TYPE_PATH && this.mapPath[square.x + "_" + square.y] !== null) {
      response = true;
    }
    return response;
  }

  getMap() {

      this.mapJSON = {
         "level":"2",
         "cases":[
            [
               {
                  "x":0,
                  "y":0,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":1,
                  "y":0,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":2,
                  "y":0,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":3,
                  "y":0,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":4,
                  "y":0,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":5,
                  "y":0,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":6,
                  "y":0,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":7,
                  "y":0,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":8,
                  "y":0,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":9,
                  "y":0,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               }
            ],
            [
               {
                  "x":0,
                  "y":1,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":1,
                  "y":1,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":2,
                  "y":1,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":3,
                  "y":1,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":4,
                  "y":1,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":5,
                  "y":1,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":6,
                  "y":1,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":7,
                  "y":1,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":8,
                  "y":1,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":9,
                  "y":1,
                  "type":"begin",
                  "cssClass":"texture path-1"
               }
            ],
            [
               {
                  "x":0,
                  "y":2,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":1,
                  "y":2,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":2,
                  "y":2,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":3,
                  "y":2,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":4,
                  "y":2,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":5,
                  "y":2,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":6,
                  "y":2,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":7,
                  "y":2,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":8,
                  "y":2,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":9,
                  "y":2,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               }
            ],
            [
               {
                  "x":0,
                  "y":3,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":1,
                  "y":3,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":2,
                  "y":3,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":3,
                  "y":3,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":4,
                  "y":3,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":5,
                  "y":3,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":6,
                  "y":3,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":7,
                  "y":3,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":8,
                  "y":3,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":9,
                  "y":3,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               }
            ],
            [
               {
                  "x":0,
                  "y":4,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":1,
                  "y":4,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":2,
                  "y":4,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":3,
                  "y":4,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":4,
                  "y":4,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":5,
                  "y":4,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":6,
                  "y":4,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":7,
                  "y":4,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":8,
                  "y":4,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":9,
                  "y":4,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               }
            ],
            [
               {
                  "x":0,
                  "y":5,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":1,
                  "y":5,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":2,
                  "y":5,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":3,
                  "y":5,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":4,
                  "y":5,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":5,
                  "y":5,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":6,
                  "y":5,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":7,
                  "y":5,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               },
               {
                  "x":8,
                  "y":5,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":9,
                  "y":5,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               }
            ],
            [
               {
                  "x":0,
                  "y":6,
                  "type":"end",
                  "cssClass":"texture path-1"
               },
               {
                  "x":1,
                  "y":6,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":2,
                  "y":6,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":3,
                  "y":6,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":4,
                  "y":6,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":5,
                  "y":6,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":6,
                  "y":6,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":7,
                  "y":6,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":8,
                  "y":6,
                  "type":"path",
                  "cssClass":"texture path-2"
               },
               {
                  "x":9,
                  "y":6,
                  "type":"stone",
                  "cssClass":"texture grass-1"
               }
            ]
         ],
         "monsterPath":[
            {
               "x":9,
               "y":1,
               "type":"begin",
               "cssClass":"texture path-1"
            },
            {
               "x":8,
               "y":1,
               "type":"path"
            },
            {
               "x":7,
               "y":1,
               "type":"path"
            },
            {
               "x":6,
               "y":1,
               "type":"path"
            },
            {
               "x":5,
               "y":1,
               "type":"path"
            },
            {
               "x":4,
               "y":1,
               "type":"path"
            },
            {
               "x":3,
               "y":1,
               "type":"path"
            },
            {
               "x":2,
               "y":1,
               "type":"path"
            },
            {
               "x":2,
               "y":2,
               "type":"path"
            },
            {
               "x":2,
               "y":3,
               "type":"path"
            },
            {
               "x":2,
               "y":4,
               "type":"path"
            },
            {
               "x":3,
               "y":4,
               "type":"path"
            },
            {
               "x":4,
               "y":4,
               "type":"path"
            },
            {
               "x":5,
               "y":4,
               "type":"path"
            },
            {
               "x":6,
               "y":4,
               "type":"path"
            },
            {
               "x":7,
               "y":4,
               "type":"path"
            },
            {
               "x":8,
               "y":4,
               "type":"path"
            },
            {
               "x":8,
               "y":5,
               "type":"path"
            },
            {
               "x":8,
               "y":6,
               "type":"path"
            },
            {
               "x":7,
               "y":6,
               "type":"path"
            },
            {
               "x":6,
               "y":6,
               "type":"path"
            },
            {
               "x":5,
               "y":6,
               "type":"path"
            },
            {
               "x":4,
               "y":6,
               "type":"path"
            },
            {
               "x":3,
               "y":6,
               "type":"path"
            },
            {
               "x":2,
               "y":6,
               "type":"path"
            },
            {
               "x":1,
               "y":6,
               "type":"path"
            },
            {
               "x":0,
               "y":6,
               "type":"end"
            }
        ],
         "waveMonster": [
             {
                 "timer": 2500,
                 "listMonster": [
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    }
                 ]
             },
             {
                 "timer": 2000,
                 "listMonster": [
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    }
                 ]
             },
             {
                 "timer": 2000,
                 "listMonster": [
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "2",
                        "health": 100
                    },
                    {
                        "type": "2",
                        "health": 100
                    },
                    {
                        "type": "1",
                        "health": 50
                    }
                 ]
             },
             {
                 "timer": 2000,
                 "listMonster": [
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "2",
                        "health": 100
                    },
                    {
                        "type": "2",
                        "health": 100
                    },
                    {
                        "type": "2",
                        "health": 100
                    },
                    {
                        "type": "2",
                        "health": 100
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    },
                    {
                        "type": "1",
                        "health": 50
                    }
                 ]
             }
         ]
      }
      this.initMap();

  }

  /**
   * Find next square path
   * @param  {Square} currentSquare [description]
   * @return {Square} next square
   */
  private findNextSquarePath(currentSquare: Square) {
    let indexSquare = this.mapPath.findIndex( (square) => square.x === currentSquare.x
      && square.y === currentSquare.y);
    let nextSquare = null;
    // If next square exist, get this
    if (indexSquare + 1 !== this.mapPath.length) {
      nextSquare = this.mapPath[indexSquare + 1];
    }
    return nextSquare;
  }

  /**
   * Assign begin square to squareBegin
   * @param  {QueryList <SquareComponent>} listSquare <SquareComponent>
   */
  private findBeginSquare (listSquare: QueryList <SquareComponent>) {
      this.beginSquareComponent = listSquare.filter( (square) => square.square.type === SquareEnum.TYPE_BEGIN )[0]
      this.beginSquareComponent.attachMonster(this.listMonster[0]);
  }

  /**
   * Remove monster
   * @param  {Monster} monster [description]
   * @return {[type]}          [description]
   */
  removeMonster(monster: Monster) {
    let index = this.listMonster.indexOf(monster);
    this.listMonster.splice(index);
  }

  /**
   * Push monster to begin square and stop if all attached
   */
  pushMonsterInBeginSquare() {
    // Push monster in begin square
    let monster = this.listMonster[this.cptMonster];
    this.beginSquareComponent.attachMonster(monster);
    // this.removeMonster(monster);
    this.cptMonster++;
    // If all monster is attached, stop timer
    if (this.cptMonster === this.listMonster.length) {
      this.timerMonster.unsubscribe();
    }
  }

}

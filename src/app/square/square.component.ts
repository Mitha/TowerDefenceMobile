import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Character} from '../character/character';
import {CharacterComponent} from '../character/character.component';
import {Fire} from '../character/fire';
import {MonsterComponent} from '../monster/monster.component';
import {Monster} from '../monster/monster';
import {MonsterSound} from '../monster/monster.sound';
import {Square} from '../square/square';
import {Subject, Observable} from "rxjs";
import {DataSubject} from '../util/data.subject';
import {DataSubjectUtil} from '../util/data.subject.util';
import {SquareEnum} from './square.enum';
import {SquareService} from './square.service';
import {CharacterService} from '../character/character.service';
import {GameService} from '../game/game.service';
import {MapService} from '../map/map.service';

const DAMAGE_CHARACTER = 20;

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.css'],
  providers: [DataSubjectUtil, SquareService, CharacterService],
})
export class SquareComponent implements OnInit {

  // Bind of param in component MapComponent
  @Input() square: Square;

  isExistCharacter: boolean;
  listMonster = [];
  character: Character;
  characterSubject:Subject<any> = new Subject();

  @Output() renderComponent = new EventEmitter();
  // Observer Parent
  @Input() parentSubject:Subject<any>;
  monsterSubject:Subject<any> = new Subject();
  nextSquare: Square;
  assignMonsterToCharacter: Monster;
  listFire = [];
  @Input() gameService: GameService;
  @Input() mapService: MapService;

  constructor(private dataSubjectUtil: DataSubjectUtil, private squareService: SquareService, private characterService: CharacterService,
        private soundMonster: MonsterSound) {
    this.isExistCharacter = false;
    this.character = null;
    this.assignMonsterToCharacter = null;
  }

  ngOnInit() {
    // Define response to parent when I notify
    this.handleParent();
    // Notify map this square is init
    this.notifyParent(this.dataSubjectUtil.generateDataSubject("initSquare", this, null))
  }

  ngOnDestroy() {
    this.parentSubject.unsubscribe();
  }

  handleParent() {
    this.parentSubject.subscribe(event => {
      if (event.action === "timeEvent") {
        this.walkMonster();
        this.manageCharacter(event.action, null);
        this.attackAssignMonster();
      }
      else if (event.action === "assignMonster" && event.condition.x === this.square.x && event.condition.y === this.square.y) {
        // Assign direction if empty
        this.assignDirection(event.condition);
        this.attachMonster(event.data);
      }
      // If is not next square, control perimeter of monster
      else if (event.action === "checkCharacterAttachMonster" && this.assignMonsterToCharacter !== null) {
          this.manageCharacter(event.action, event.condition);
      }
      // Assign Monster proximity
      else if (event.action === "assignMonsterToNearbyCharacter" && this.character !== null) {
        if (this.squareService.isInPerimeterCharacter(this.square, event.condition) && this.assignMonsterToCharacter === null) {
          // Notify if monster is has proximity character
          this.assignMonsterToCharacter = event.data;
          this.notifyCharacterComponent(this.dataSubjectUtil.generateDataSubject("attachMonster", event.data, null))
        }
      }
      else if (event.action === "attackMonster") {
        this.attackThisMonster(event);
      }
    });
  }

  /**
   * Assign last and new direction of next path if they are empty
   * @param  {Square} square [description]
   * @return {[type]}        [description]
   */
  assignDirection(square: Square) {
    if (this.square.lastDirection === null) {
      this.square.lastDirection = square.lastDirection;
      this.square.direction = square.direction;
      this.square.nextDirection = square.nextDirection;
      this.square.directionProperties = square.directionProperties;
    }
  }

  initCharacter() {
    if (this.character === null && this.isSpawnCharacter()) {
      let character = new Character("mini", 1, 50, 0, 10, 200, false, null, null);
      let cost = this.characterService.buyCharacter(character);
      // If user has enough money
      if (this.gameService.game.money >= cost) {
          this.gameService.game.money = this.gameService.game.money - cost;
          this.character = character;
          this.character.cptWaitAttack = 20;
          this.characterSubject = new Subject();
      }
    }
  }

  addCharacter() {
    if (this.character !== null) {
      this.notifyCharacterComponent(this.dataSubjectUtil.generateDataSubject("addLevel", null, null))
    }
  }

  manageCharacter(action: string, monster: Monster) {
    if (this.character !== null) {
      // Manage direction and attack of character
      if (action === "timeEvent") {
          this.notifyCharacterComponent(this.dataSubjectUtil.generateDataSubject("manageCharacter", this.square, null))
          this.checkAssignMonsterIsDead();
      }
      // Manage assign monster
      else if (action === "checkCharacterAttachMonster") {
        // If monster assign is out perimeter, detach this
        if (monster.id === this.assignMonsterToCharacter.id) {
          let isProximity = this.squareService.isInPerimeterCharacter(this.square, this.assignMonsterToCharacter.square);
          if (!isProximity || (this.assignMonsterToCharacter.square.type === SquareEnum.TYPE_END && isProximity)) {
            this.assignMonsterToCharacter = null;
            this.notifyCharacterComponent(this.dataSubjectUtil.generateDataSubject("detachMonster", null, null))
          }
        }
      }
    }
  }

  /**
   * Check if assign monster is dead
   */
  checkAssignMonsterIsDead() {
      if (this.assignMonsterToCharacter !== null && this.assignMonsterToCharacter.health <= 0) {
          this.assignMonsterToCharacter = null;
      }
  }

  /**
   * Check if square is spawn for character
   * @return {boolean} true is spawn
   */
  isSpawnCharacter() {
    let response = false;
    if (this.square.type !== SquareEnum.TYPE_END && this.square.type !== SquareEnum.TYPE_BEGIN
        && this.square.type !== SquareEnum.TYPE_PATH) {
      response = true;
    }
    return response;
  }

  /**
   * Attach Monster to this square
   * @param  {Monster} monster
   */
  attachMonster(monster: Monster) {
    monster.square = this.square;
    this.listMonster.push(monster);
    // Update monster subject
    this.monsterSubject = new Subject();
    // Notify parent for check character to attach this monster
    this.notifyParent(this.dataSubjectUtil.generateDataSubject("checkCharacterAttachMonster", null, monster))
  }

  /**
   * Remove monster
   * @param  {Monster} monster [description]
   * @return {[type]}          [description]
   */
  removeMonster(monster: Monster) {
    let index = this.listMonster.indexOf(monster);
    this.listMonster.splice(index, 1);
  }

  walkMonster() {
    if (this.listMonster.length > 0) {
      this.notifyMonsterComponent(this.dataSubjectUtil.generateDataSubject("walkMonster", this.nextSquare, this.square));
    }
  }

  /**
   * Character attack assign monster
   */
  attackAssignMonster() {
    if (this.character !== null && this.assignMonsterToCharacter !== null) {
      this.character.cptWaitAttack--;
      if (this.character.cptWaitAttack === 0) {
          this.notifyParent(this.dataSubjectUtil.generateDataSubject("attackMonster", this.character, this.assignMonsterToCharacter))
          this.character.cptWaitAttack = 25;
      }
    }
  }

  attackThisMonster(event: DataSubject) {
    if(this.listMonster.length > 0) {
      let findMonster = this.listMonster.find( (monster) => monster.id === event.condition.id)
      if (typeof findMonster !== "undefined") {
        this.soundMonster.soundHurt();
        findMonster.health = findMonster.health - DAMAGE_CHARACTER;
        // If monster is dead - earn money
        if (findMonster.health <= 0) {
            this.removeMonster(findMonster);
            this.gameService.earnMoney(findMonster);
            this.mapService.numberMonsterKillInWave++;
        }
        // Add animate fire in square
        this.listFire.push(new Fire(30))
        // Delete fire to end animation
        setTimeout(() => {
          this.listFire.splice(0);
      }, 800)
      }
    }
  }

  notifyMonsterComponent(event) {
    this.monsterSubject.next(event)
  }

  notifyCharacterComponent(event) {
    this.characterSubject.next(event)
  }

  handleMonsterComponent(event: any) {
    if (event instanceof DataSubject) {
      // Control to monster component
      if (event.action === "controlMonster") {
        this.controlMonster(event);
      }
    }
  }

  damageMonster(monster: Monster) {
    this.notifyParent(this.dataSubjectUtil.generateDataSubject("damageMonster", null, null));
    // Notify parent for check character to attach this monster
    this.notifyParent(this.dataSubjectUtil.generateDataSubject("checkCharacterAttachMonster", null, monster))
  }

  controlMonster(event: DataSubject) {
    let monster = event.data.monster;
    if (monster.distance >= this.square.size) {
      monster[this.square.directionProperties] = null;
      monster["rotation"] = this.squareService.getDegreesNextSquare(this.square.direction);
      monster.distance = 0;
      // If next path exist
      if (this.square.type !== SquareEnum.TYPE_END) {
        this.renderComponent.emit(this.dataSubjectUtil.generateDataSubject("nextPath", monster, this.square));
      }
      // If is last square, decrement health game
      else {
        this.damageMonster(monster);
      }
      this.removeMonster(monster);
    }
    // If this square is angle
    else if (this.square.type === SquareEnum.TYPE_PATH && this.square.lastDirection !== null
      && this.square.lastDirection !== this.square.nextDirection) {
      let sizeIntervall = monster.size + this.square.padding;
      // If monster is in first part of square (size monster + padding)
      if (monster.distance <= sizeIntervall && this.square.lastDirection !== this.square.direction) {
        monster = this.changeDirectionInAngleSquare(this.square.lastDirection, monster);
      }
      // If monster is in second part of square
      else if (monster.distance > sizeIntervall && this.square.lastDirection === this.square.direction) {
        monster = this.changeDirectionInAngleSquare(this.square.nextDirection, monster);
      }
    }
  }

  /**
   * Change direction of monster in angle square
   * @param  {string}  direction to follow
   * @param  {Monster} monster   [description]
   * @return {Monster}            [description]
   */
  changeDirectionInAngleSquare(direction: string, monster: Monster) {
    let newDirection = this.squareService.convertDirectionForPropretiesCSSUse(direction);
    this.square.directionProperties = newDirection;
    this.square.direction = direction;
    monster.rotation = this.squareService.getDegreesNextSquare(direction);
    return monster;
  }

  notifyParent(event: DataSubject) {
    this.renderComponent.emit(event);
  }

  searchMonsterProximity() {
    this.notifyParent(this.dataSubjectUtil.generateDataSubject("monsterNearby", null, this.square));
  }

}

import { Injectable } from '@angular/core';
import {Square} from '../square/square';
import {SquareEnum} from '../square/square.enum';

@Injectable()
export class SquareService {

  STEP: number = 3;

  constructor() {}

  /**
   * Get direction of next square
   * @param  {Square} currentSquare [description]
   * @param  {Square} nextSquare    [description]
   * @return {string} direction of next square
   */
  getDirectionNextSquare(currentSquare: Square, nextSquare: Square) {
    let direction = SquareEnum.DIRECTION_RIGHT;
    if (nextSquare.x < currentSquare.x) {
      direction = SquareEnum.DIRECTION_LEFT;
    }
    else if (nextSquare.y < currentSquare.y) {
      direction = SquareEnum.DIRECTION_TOP;
    }
    else if (nextSquare.y > currentSquare.y) {
      direction = SquareEnum.DIRECTION_BOTTOM;
    }
    return direction;
  }

  /**
   * Get degrees for next square
   * @param  {string} direction [description]
   * @return {number} degrees
   */
  getDegreesNextSquare(direction: string) {
    let deg = 0;
    if (direction === SquareEnum.DIRECTION_LEFT) {
      deg = 180;
    }
    else if (direction === SquareEnum.DIRECTION_TOP) {
      deg = 270;
    }
    if (direction === SquareEnum.DIRECTION_BOTTOM) {
      deg = 90;
    }
    return deg;
  }

  convertDirectionForPropretiesCSSUse(direction) {
    let newDirection = SquareEnum.DIRECTION_LEFT;
    if (direction === SquareEnum.DIRECTION_TOP || direction === SquareEnum.DIRECTION_BOTTOM) {
      newDirection = SquareEnum.DIRECTION_TOP;
    }
    return newDirection;
  }

  convertSizeDirection(direction: string, distance: number) {
    let newDistance = distance + this.STEP;
    if (direction === SquareEnum.DIRECTION_LEFT) {
      newDistance = distance - this.STEP;
    }
    else if (direction === SquareEnum.DIRECTION_TOP) {
      newDistance = distance - this.STEP;
    }
    else if (direction === SquareEnum.DIRECTION_BOTTOM) {
      newDistance = distance + this.STEP;
    }
    return newDistance;
  }

  /**
   * Initialisation start position of monster in square
   * @param  {string} direction [description]
   * @param  {number} size      [description]
   * @return {number} position of monster
   */
  initStartPositionOfMonsterInSquare(direction: string, size: number) {
    let position = size;
    if (direction === SquareEnum.DIRECTION_RIGHT || direction === SquareEnum.DIRECTION_BOTTOM) {
      position = - size;
    }
    return position;
  }

  /**
   * Know if monster is nearby character
   * @param  {Square} currentSquare [description]
   * @param  {Square} squareMonster [description]
   * @return true if monster is nearby character
   */
  isInPerimeterCharacter(currentSquare: Square, squareMonster: Square) {
    let response = false;
    if ((currentSquare.x === squareMonster.x + 1 || currentSquare.x === squareMonster.x - 1 || currentSquare.x === squareMonster.x)
        && (currentSquare.y === squareMonster.y + 1 || currentSquare.y === squareMonster.y - 1 || currentSquare.y === squareMonster.y)) {
      response = true;
    }
    return response;
  }

}

export class Square {

  type: string;
  x: number;
  y: number;
  size: number;
  padding: number;
  direction: string;
  lastDirection: string;
  nextDirection: string;
  directionProperties: string;
  style: string;

  constructor (type: string, x: number, y: number, size: number, padding: number, style: string) {
    this.type = type;
    this.x = x;
    this.y = y;
    this.size = size;
    this.padding = padding;
    this.lastDirection = null;
    this.nextDirection = null;
    this.directionProperties = null;
    this.style = style;
  }

}

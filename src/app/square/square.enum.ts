export const SquareEnum =  {
    TYPE_PATH: "path",
    TYPE_BEGIN: "begin",
    TYPE_END: "end",
    TYPE_STONE: "stone",
    DIRECTION_LEFT: "left",
    DIRECTION_TOP: "top",
    DIRECTION_BOTTOM: "bottom",
    DIRECTION_RIGHT: "right"
}

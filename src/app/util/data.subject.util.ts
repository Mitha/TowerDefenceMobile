import { Injectable } from '@angular/core';
import {DataSubject} from './data.subject';

@Injectable()
export class DataSubjectUtil {

  constructor() {}

  /**
   * Generate DataSubect for communication with many component
   * @param  {string} action    [description]
   * @param  {any}    data      [description]
   * @param  {any}    condition [description]
   * @return {DataSubject}
   */
  generateDataSubject(action: string, data: any, condition: any) {
    return new DataSubject(action, data, condition);
  }

}

export class DataSubject {

  action: string;
  data: any;
  condition: any;

  constructor(action: string, data: any, condition: any) {
    this.action = action;
    this.data = data;
    this.condition = condition;
  }

}

export class Character {

  type: string;
  level: number;
  damage: number;
  rotation: number;
  speed: number;
  distance: number;
  select: boolean;
  position: number;
  style: string;
  cptWaitAttack: number;

  constructor (type: string, level: number, damage: number,
    rotation: number, speed: number, distance: number,
    select: boolean, position: number, style: string) {
    this.type = type;
    this.level = level;
    this.damage = damage;
    this.rotation = 0;
    this.speed = speed;
    this.distance = distance;
    this.select = select;
    this.position = position;
    this.style = style;
  }

}

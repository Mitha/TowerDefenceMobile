export class Fire {

  id: string;
  left: number;
  top: number;
  size: number;

  constructor (size: number) {
    this.size = size;
  }

}

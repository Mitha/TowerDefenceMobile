import { Component, OnInit, Input, Injectable, Output, EventEmitter } from '@angular/core';
import {Monster} from '../monster/monster';
import {Square} from '../square/square';
import {Character} from '../character/character';
import {CharacterService} from '../character/character.service';
import {Subject} from "rxjs";
import {DataSubject} from '../util/data.subject';
import {DataSubjectUtil} from '../util/data.subject.util';

const REFRESH_TIME = 40;
const MAX_LEVEL = 3;

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent implements OnInit {

  @Input() character : Character;
  @Input() parentSubject:Subject<any>;
  @Output() notifyParent = new EventEmitter();
  assignMonster: Monster;
  @Input() square: Square;

  constructor(private dataSubjectUtil: DataSubjectUtil, private characterService: CharacterService) {
    this.assignMonster = null;
  }

  ngOnInit() {
    this.handleParent();
  }

  handleParent() {
    this.parentSubject.subscribe(event => {
      if (event.action === "addLevel") {
        this.addLevel();
      }
      else if (event.action === "attackMonster") {
        if (this.assignMonster !== null) {
          this.attackMonster();
        } else {
          this.assignMonster = event.data;
        }
      }
      else if (event.action === "attachMonster") {
        this.assignMonster = event.data;
      }
      else if (event.action === "detachMonster") {
        this.assignMonster = null;
      }
      else if (event.action === "manageCharacter" && this.assignMonster !== null) {
        let angle = this.characterService.calculateAngleDegrees(this.square, this.assignMonster.square, this.assignMonster);
        this.character.rotation = angle;
      }
    });
  }

  controlleCharacter() {

  }

  changePosition() {

  }

  eventTime() {
    setTimeout(this.controlleCharacter(), REFRESH_TIME);
  }

  addLevel() {
    if (this.character.level < MAX_LEVEL) {
      this.character.level++;
      this.character.style = this.getClassStyleCharacter();
    }
  }

  getClassStyleCharacter() {
    return "character -" + this.character.type + "-" + this.character.level;
  }

  initCharacter() {
    this.character.type = "mini";
    this.character.level = 1;
    this.character.distance = 200;
  }

  attackMonster() {

  }

}

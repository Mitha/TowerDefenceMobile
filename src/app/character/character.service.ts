import { Injectable } from '@angular/core';
import {Square} from '../square/square';
import {Monster} from '../monster/monster';
import {Character} from '../character/character';
import {SquareEnum} from '../square/square.enum';

const COST_CHARACTER_MINI_1 = 50;
const COST_CHARACTER_MINI_2 = 100;
const COST_CHARACTER_MINI_3 = 200;
const TYPE_MINI = "mini";

@Injectable()
export class CharacterService {

  constructor() {}

  /**
   * Calculate angle to character for monster
   * @param  {Square}  currentSquare [description]
   * @param  {Square}  ciblingSquare [description]
   * @param  {Monster} monster       [description]
   * @return {number} degrees
   */
  calculateAngleDegrees(currentSquare: Square, ciblingSquare: Square, monster: Monster) {
    let x = ciblingSquare.x;
    let y = ciblingSquare.y;
    // Calculate marge to remove at direction character
    let margeMonster = (currentSquare.size / 2) + (monster.size / 2);
    // Calculate position of monster in square
    let temp = (monster.distance - margeMonster) / currentSquare.size;
    // Manage direction of character by monster direction
    if (ciblingSquare.direction === SquareEnum.DIRECTION_RIGHT) {
      x = x + temp;
    }
    else if (ciblingSquare.direction === SquareEnum.DIRECTION_LEFT) {
      x = x - temp;
    }
    else if (ciblingSquare.direction === SquareEnum.DIRECTION_TOP) {
      y = y - temp;
    }
    else if (ciblingSquare.direction === SquareEnum.DIRECTION_BOTTOM) {
      y = y + temp;
    }
    // Calculate angle
    let angle = Math.atan2(currentSquare.x - x, currentSquare.y - y) * 180 / Math.PI;
    return -angle;
  }

  /**
   * Get price of character
   * @param  {Character} character [description]
   * @return {number}              [description]
   */
  buyCharacter(character: Character) {
      let price = COST_CHARACTER_MINI_1;
      // Type mini
      if (character.type === TYPE_MINI) {
          if (character.level === 2) {
              price = COST_CHARACTER_MINI_2;
          }
          else if (character.level === 3) {
              price = COST_CHARACTER_MINI_3;
          }
      }
      return price;
  }

}

import { Injectable } from '@angular/core';

@Injectable()
export class MonsterSound {

  constructor() {}

  soundHurt() {
      let audio = new Audio("./sound/hurt.mp3");
      audio.play();
  }

}

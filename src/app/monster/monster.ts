import {Square} from '../square/square';

export class Monster {

  id: string;
  type: string;
  level: number;
  rotation: number;
  speed: number;
  position: number;
  health: number;
  top: number;
  left: number;
  bottom: number;
  right: number;
  size: number;
  distance: number;
  square: Square;

  constructor (type: string, level: number, health: number,
    rotation: number, speed: number, position: number) {
    this.type = type;
    this.level = level;
    this.rotation = rotation;
    this.speed = speed;
    this.position = position;
    this.health = health;

    this.top = 0;
    this.left = 0;
    this.right = 0;
    this.bottom = 0;
    this.distance = 0;
    this.id = this.generatorID();
    this.square = null;
  }

  getRotateCSS() {
    return "rotate(" + this.rotation + "deg)";
  }

  /**
   * Generate ID of monster
   * @return {[type]} [description]
   */
  generatorID() {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
  }

}

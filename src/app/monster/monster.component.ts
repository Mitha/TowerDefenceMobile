import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Monster} from './monster';
import {Subject} from "rxjs";
import {DataSubject} from '../util/data.subject';
import {DataSubjectUtil} from '../util/data.subject.util';
import {SquareService} from '../square/square.service';

@Component({
  selector: 'app-monster',
  templateUrl: './monster.component.html',
  styleUrls: ['./monster.component.css']
})
export class MonsterComponent implements OnInit {

  @Input() monster: Monster;
  @Output() notifyParent = new EventEmitter();
  @Input() parentSubject:Subject<any>;
  responseParent: string;

  constructor(private dataSubjectUtil: DataSubjectUtil, private squareService: SquareService) {

  }

  ngOnInit() {
    this.handleParent();
  }

  ngOnDestroy() {
    this.parentSubject.unsubscribe();
  }

  handleParent() {
    this.parentSubject.subscribe(event => {
      if (event.action === "walkMonster") {
        // console.log("walkMonster :", event.condition.directionProperties)
        // Change properties left or top to monster in square
        this.monster[event.condition.directionProperties] = this.squareService.convertSizeDirection(event.condition.direction,
           this.monster[event.condition.directionProperties])
        // console.log("left: ", this.monster.left, "top :", this.monster.top)
        // Increment distance of monster in square
        this.monster.distance = this.monster.distance + this.squareService.STEP;
        if (this.parentSubject.observers.length > 0) {
          // Notify parent monster has changed for controll this
          this.notifyParent.next(this.dataSubjectUtil.generateDataSubject("controlMonster", this, null));
        }
      }
    });
  }

}

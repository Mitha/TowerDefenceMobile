import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import {Subject} from "rxjs";
import {DataSubject} from '../util/data.subject';
import {DataSubjectUtil} from '../util/data.subject.util';

@Component({
  selector: 'app-menu-character',
  templateUrl: './menu-character.component.html',
  styleUrls: ['./menu-character.component.css']
})
export class MenuCharacterComponent implements OnInit {

    @Input() parentSubject:Subject<any>;
    @Output() notifyParent = new EventEmitter();
    visibility: string;

    constructor(private dataSubjectUtil: DataSubjectUtil) {
        this.visibility = null;
    }

    showMenu() {
        this.visibility = "visible"
    }

    ngOnInit() {
    }

    handleParent() {
      this.parentSubject.subscribe(event => {

      });
    }

    notifyParents(event: DataSubject) {
      this.notifyParent.emit(event);
    }

}

export class Game {

  money: number;
  health: number;
  wave: number;
  timerWait: number;

  constructor (money: number, health: number) {
    this.money = money;
    this.health = health;
    this.wave = 1;
    this.timerWait = 0;
  }

}

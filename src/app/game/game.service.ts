import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Game} from './game';
import {Monster} from '../monster/monster';
import {MonsterEnum} from '../monster/monster.enum';

@Injectable()
export class GameService {

    game: Game;

    constructor() {

    }

    ngOnInit() {
    }

    earnMoney(monster: Monster) {
        let money = 20;
        if (monster.type === MonsterEnum.MONSTER_2) {
            money = 30;
        }
        else if (monster.type === MonsterEnum.MONSTER_3) {
            money = 40;
        }
        this.game.money = this.game.money + money;
    }

}

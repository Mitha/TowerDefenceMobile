import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import {MapComponent} from '../map/map.component';
import {MapService} from '../map/map.service';
import {CharacterComponent} from '../character/character.component';
import {MonsterComponent} from '../monster/monster.component';
import {DataSubject} from '../util/data.subject';
import {DataSubjectUtil} from '../util/data.subject.util';
import {SquareService} from '../square/square.service';
import {GameService} from './game.service';
import {Game} from './game';
import {Subject, Observable} from "rxjs";

const START_MONEY = 50;
const HEALTH = 5;

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
  providers: [MapService, DataSubjectUtil, SquareService]
})
export class GameComponent implements OnInit {

  listCharacter = [];
  listMonster = [];
  listPath = [];
  mapSubject:Subject<any> = new Subject();
  successGame: boolean;

  constructor(private dataSubjectUtil: DataSubjectUtil, private gameService: GameService) {
    this.gameService.game = new Game(START_MONEY, HEALTH);
    this.successGame = false;
  }

  ngOnInit() {
  }

  handleChildMap(event: DataSubject) {
    if (event.action === "damageMonster") {
      this.gameService.game.health--;
      // If health is null, stop game
      if (this.gameService.game.health === 0) {
        this.notifyMap(this.dataSubjectUtil.generateDataSubject("endGame", null, null))
      }
    }
    else if (event.action === "successGame") {
        this.successGame = true;
    }
  }

  notifyMap(event) {
    this.mapSubject.next(event);
  }



}

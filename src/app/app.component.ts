import { Component } from '@angular/core';
import {DataSubject} from './util/data.subject';
import {DataSubjectUtil} from './util/data.subject.util';
import {GameService} from './game/game.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DataSubjectUtil, GameService]
})
export class AppComponent {
  title = 'Tower Defence';
}

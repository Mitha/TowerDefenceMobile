// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

const BASE_URL = 'http://localhost:3000'
const TOWER_SERVER_URL = "http://localhost:4201"

export const environment = {
  production: false,
  map_url_1 : `${TOWER_SERVER_URL}/maps/1`,
  map_url_2 : `${TOWER_SERVER_URL}/maps/2`,
  map_url_3 : `${TOWER_SERVER_URL}/maps/3`,
  map_url : `${BASE_URL}/towerDefence`,
  envName: 'Develop',
};

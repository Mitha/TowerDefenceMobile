const BASE_URL = 'https://localhost:3000'

export const environment = {
  production: true,
  map_url : `${BASE_URL}/towerDefence`,
  envName: 'Production',
};

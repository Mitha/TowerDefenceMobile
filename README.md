Angular 2 :
https://github.com/angular/angular-cli

TODOLIST - FRONT :

- Create template Page
- Create Component Map
  - Features BitMap
  - Generate Doc
  - Feature Image Map
  - Feature Walk
- Create Component Spawn
- Create Component Monster
  - Add attributs :
    - Health
    - Speed
  - Feature Walk Monster
- Create Component Defence
  - Feature Fire
  - Feature Level
  - Add attributs :
    - Attack
    - Speed
    - Distance

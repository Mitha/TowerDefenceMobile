FROM node
RUN mkdir /usr/src/towerdefence
WORKDIR /usr/src/towerdefence
RUN npm install -gq @angular/cli@latest
ADD package.json .
RUN npm install
ADD . .
EXPOSE 4200
CMD ["npm","run","start"]

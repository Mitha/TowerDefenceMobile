import { TowerDefencePage } from './app.po';

describe('tower-defence App', () => {
  let page: TowerDefencePage;

  beforeEach(() => {
    page = new TowerDefencePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
